package design.adapter.list.alpha;

public class AlphaListManager {
    public static AlphaList createList() {
        return new AlphaList() {
            @Override
            public int get(int index) {
                return 0;
            }
            @Override
            public int count() {
                return 0;
            }
            @Override
            public void add(int value) {}
            @Override
            public void set(int index, int value) {}
            @Override
            public void remove(int index) {}
        };
    }
}
