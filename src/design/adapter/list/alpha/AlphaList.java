package design.adapter.list.alpha;

public interface AlphaList {
    int get(int index);
    int count();
    void add(int value);
    void set(int index, int value);
    void remove(int index);
}
