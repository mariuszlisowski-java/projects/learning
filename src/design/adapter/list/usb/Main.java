package design.adapter.list.usb;

public class Main {

    public static void main(String[] args) {
        MemoryCard memoryCard = new MemoryCard();

        CardReader cardReader = new CardReader(memoryCard);
        cardReader.connectWithUSBCable();
    }

}
