package design.adapter.list.usb;

public class CardReader implements USB {
    private MemoryCard memoryCard;

    public CardReader(MemoryCard memoryCard) {
        this.memoryCard = memoryCard;
    }

    @Override
    public void connectWithUSBCable() {
        memoryCard.insert();
        memoryCard.copyData();
    }
}
