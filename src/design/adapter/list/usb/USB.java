package design.adapter.list.usb;

public interface USB {
    void connectWithUSBCable();
}
