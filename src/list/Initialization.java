package list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Initialization {

    public static void initializeListAsStaticInitializer() {
        ArrayList<Integer> arrayList = new ArrayList<Integer>() {{
            add(2);
            add(4);
            add(8);
        }};
    }

    public static void initializeListAsArrays() {
        ArrayList<String> stringArrayList = new ArrayList<>(
                Arrays.asList("one", "two", "three"));
        // even simpler
        List<String> stringList = Arrays.asList("one", "two", "three");
    }
}
