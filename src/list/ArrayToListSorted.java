package list;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ArrayToListSorted {
    public static void main(String[] args) {
        int[] arr = { 1, 2, 3, 4, 5 };

        List<Integer> list1 = arrayToListReverseSorted(arr);
        System.out.println(list1);
        List<Integer> list2 = arrayToListNaturalSorted(arr);
        System.out.println(list2);
    }

    public static List<Integer> arrayToListReverseSorted(int[] array) {
        return Arrays.stream(array).boxed().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
    }

    public static List<Integer> arrayToListNaturalSorted(int[] array) {
        return Arrays.stream(array).boxed().sorted(Comparator.naturalOrder()).collect(Collectors.toList());
    }

}
