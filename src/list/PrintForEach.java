package list;

import javax.sound.midi.Soundbank;
import java.util.ArrayList;
import java.util.List;

public class PrintForEach {
    public static void main(String[] args) {
        List<Integer> ints = new ArrayList<>() {{
            add(2);
            add(4);
            add(6);
        }};

        printForEach(ints);
        printLambda(ints);
    }

    private static void printForEach(List<Integer> list) {
        list.forEach(System.out::println);
    }

    private static void printLambda(List<Integer> list) {
        list.forEach(value -> System.out.print(value + " "));
    }
}
