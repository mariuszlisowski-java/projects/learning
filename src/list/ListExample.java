package list;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class ListExample {
    public static void main(String[] args) {
        List<String> list = new Vector<>();
        list.add("one");
        list.add("two");
        list.add("three");

        // iterator
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            String text = it.next();
            System.out.println(text);
        }

        // whole list
        System.out.println(list);

        // foreach
        for (String el : list) {
            System.out.println(el);
        }
    }
}
