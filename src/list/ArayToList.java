package list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ArayToList {
    public static void main(String[] args) {
        // initialize
        List<Integer> list1 = Arrays.asList(1, 2, 3, 4, 5);
        System.out.println(list1);

        int[] arr = { 1, 2, 3, 4, 5 };

        // loop
        List<Integer> list2 = new ArrayList<>(arr.length);
        for (int el : arr) {
            list2.add(el);
        }
        System.out.println(list2);

        // collectors
        List<Integer> list3 = arrayToList(arr);
        System.out.println(list3);
    }

    public static List<Integer> arrayToList(int[] array) {
        return Arrays.stream(array).boxed().collect(Collectors.toList());
    }

}
