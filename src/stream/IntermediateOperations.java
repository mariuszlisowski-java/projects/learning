package stream;

import java.util.stream.Stream;

// map(), filter(), distinct(), sorted(), limit(), skip()
public class IntermediateOperations {
    public static void main(String[] args) {
        Stream<Integer> ints = Stream.of(5, 4, 3, 2, 1, 1, 0, -1);

        ints.distinct()                         // no duplicates
                .sorted()                       // ascending
                .filter(el -> el >= 0)          // non negative
                .skip(1)                        // skip first element (zero)
                .limit(5)                       // max length of stream
                .forEach(System.out::println);  // print all remaining
    }
}
