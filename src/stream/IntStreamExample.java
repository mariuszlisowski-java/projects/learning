package stream;

import java.util.Arrays;
import java.util.Comparator;
import java.util.OptionalDouble;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class IntStreamExample {
    public static void main(String[] args) {
        IntStream intStream = Arrays.stream(
                new int[] {1, 2, 3, 4});    // int

        OptionalDouble average = intStream.average();
        average.ifPresent(System.out::println); // stream terminated

        Stream ints = Stream.of(1, 2, 3, 4); // Integer
        ints.max(Comparator.naturalOrder())
            .ifPresent(System.out::println);
    }
}
