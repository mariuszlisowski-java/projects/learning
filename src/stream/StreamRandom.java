package stream;

import java.util.Random;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class StreamRandom {
    public static void main(String[] args) {
        Random random = new Random();
        IntStream intStream = random.ints(10,50, 60);

        intStream.forEach(el -> System.out.print(el + ", "));
    }
}
