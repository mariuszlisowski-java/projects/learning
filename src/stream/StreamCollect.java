package stream;

import javax.annotation.processing.SupportedSourceVersion;
import java.util.*;
import java.util.stream.Collectors;

public class StreamCollect {

    public static void main(String[] args) {
        List<Product> productList = Arrays.asList(
                new Product(23, "potatoes"),
                new Product(14, "orange"),
                new Product(13, "lemon"),
                new Product(23, "bread"),
                new Product(13, "sugar")
        );

        // collecting to list
        String listedToString = productList.stream()
                .map(Product::getName)
                .collect(Collectors.joining(", ", "[", "]"));
        System.out.println(listedToString);

        // collecting to string
        List<String> names = productList.stream()
                .map(Product::getName)
                .collect(Collectors.toList());
        names.stream().forEach(el -> System.out.print(el + ", "));

        // collecting average
        double averageQuantity = productList.stream()
                .collect(Collectors.averagingInt(Product::getQuantity));
        System.out.println("\n" + averageQuantity);

        // collecting total of
        int summedQuantity = productList.stream()
                .collect(Collectors.summingInt(Product::getQuantity));
        System.out.println(summedQuantity);

        // collecting statistics
        IntSummaryStatistics statistics = productList.stream()
                .collect(Collectors.summarizingInt(Product::getQuantity));
        System.out.println(statistics);

        // grouping according to function
        Map<Integer, List<Product>> mapOfProducts = productList.stream()
                .collect(Collectors.groupingBy(Product::getQuantity));
        System.out.println(mapOfProducts); // products of the same quantity

        // grouping according to predicate
        Map<Boolean, List<Product>> mapPartitioned = productList.stream()
                .collect(Collectors.partitioningBy(el -> el.getQuantity() > 10));

        // convert to unmodifiable set
        Set<Product> unmodifiableSet = productList.stream()
                .collect(Collectors.collectingAndThen(
                            Collectors.toSet(),
                            Collections::unmodifiableSet)
                        );
    }

    static class Product {
        private int quantity;
        private String name;

        public Product(int quantity, String name) {
            this.quantity = quantity;
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public int getQuantity() {
            return quantity;
        }
    }
}
