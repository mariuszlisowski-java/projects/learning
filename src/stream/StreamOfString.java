package stream;

import java.util.regex.Pattern;
import java.util.stream.Stream;

public class StreamOfString {
    public static void main(String[] args) {
        Stream<String> strings =
                Pattern.compile(", ").splitAsStream("first, second, third");
        strings.forEach(System.out::println);
    }
}
