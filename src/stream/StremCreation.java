package stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StremCreation {
    public static void main(String[] args) {
        List<Integer> ints =new ArrayList<>(
                Arrays.asList(1, 2, 3, 2, 1));

        // .stream()
        List<Integer> distinctInts = ints.stream()
                .distinct()  // intermediate operation
                .collect(Collectors.toList());  // terminal operation
        distinctInts.forEach(System.out::print);

        List<Integer> largerThanTwo = ints.stream()
                .filter(element -> element > 2)  // intermediate operation
                .collect(Collectors.toList());  // terminal operation
        largerThanTwo.forEach(System.out::print);

        // Stream.of()
        Stream.of(ints)
                .forEach(System.out::println);
        Stream<Character> charStream = Stream.of('a', 'b', 'c');
        charStream.forEach(System.out::print);

        // Stream.builder()
        Stream<String> stringStream = Stream.<String>builder()
                .add("ala") // intermediate operation
                .add("ma")
                .add("kota")
                .build(); // terminal operation
    }
}
