package stream;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class IntStreamRange {
    public static void main(String[] args) {
        // primitive (int)
        int sum = IntStream.range(1, 5).sum(); // [1, 5), e.g. [1, 4]
        System.out.println("Sum: " + sum);

        // objects (Integer)
        Stream.of(4, 2, 1, 3)
                .sorted()
                .forEach(val -> System.out.print(val + " "));
    }
}
