package stream.persons;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {
    public static void main(String... args) {
        // filtering
        var persons = getPersons();
        persons.stream()
                .filter(person -> {
                    System.out.print("Filtering adults... ");
                    return person.getAge() >= 18;
                })
                .forEach(person -> System.out.println(person.getName()));

        // sorting
        var people = getPersons();
        List<Person> sortedPeople = persons.stream()
                .filter(person -> person.getAge() >= 18)
                .sorted(Comparator.comparing(Person::getName))
                .collect(Collectors.toList());
        sortedPeople.forEach(System.out::println);

        // adding other types to stream
        var beings = getPersons();
        beings.stream()
                .filter(person -> person.getAge() >= 18)
                .flatMap(person -> Stream.of(person, "is adult", 18, '!')) // or Arrays.asList().stream()
                .forEach(object -> {
                    if (object instanceof Person) {
                        System.out.print(
                                ((Person) object).getName() + " ");
                    } else if (object instanceof String){
                        System.out.print(object);
                    } else if (object instanceof Integer){
                        System.out.print(" | age >= " + object);
                    } else {
                        System.out.println(object); // char
                    }
                });

        // adding same type to stream
        var students = getPersons();
        students.stream()
                .flatMap(person -> Arrays.asList(person, new Person("Leo", 23)).stream())
                .forEach(System.out::println);
    }

    public static List<Person> getPersons() {
        return List.of(
                new Person("Emily", 25),
                new Person("Kate", 18),
                new Person("Kim", 43),
                new Person("Alice", 33));
    }
}
