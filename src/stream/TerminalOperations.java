package stream;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// forEach(), toArray(), reduce(), collect(), min(), max(), count(),
// anyMatch(), allMatch(), noneMatch(), findFirst(), findAny()
public class TerminalOperations {
    public static void main(String[] args) {
        Stream<Integer> ints = Stream.of(1, 2, 3);
        Object[] array = ints.toArray(); // stream terminated (cannot be reused)

        List<Integer> nums = Arrays.asList(5, 4, 3, 2, 1, 0);
        // .collect()
        Set<Integer> integerList =
                nums.stream()
                    .limit(3)
                    .collect(Collectors.toSet());
        // .findAny()
        Optional<Integer> result =
                nums.stream()
                    .findAny();
        result.ifPresent(System.out::println);
        // .count()
        long n = nums.stream()
                .filter(el -> el > 2)
                .peek(System.out::print)
                .count();
        // .max()
        Optional<Integer> maximum = nums.stream()
                .max(Comparator.naturalOrder());
        // .min()
        Optional<Integer> minimum = nums.stream()
                .min(Comparator.naturalOrder());
        // .forEach()
        nums.stream().forEach(System.out::print);
    }
}
