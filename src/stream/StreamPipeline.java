package stream;

import java.util.stream.Stream;

public class StreamPipeline {
    public static void main(String[] args) {
        Stream<String> skippedStream =
                Stream.of("ab", "cd", "ef").skip(1);    // not terminated (can be reused)
        skippedStream = skippedStream.skip(1);          // intermediate (again)
        skippedStream.forEach(System.out::println);     // terminated

        Stream<String> stringStream =
                Stream.of("12.32", "11.23", "10.34");   // created
        stringStream.map(el -> el.substring(0, 2))
                .sorted()
                .forEach(System.out::println);          // terminated
    }
}
