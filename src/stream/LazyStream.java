package stream;

import java.util.stream.IntStream;

public class LazyStream {
    public static void main(String[] args) {
        IntStream ints = IntStream.of(1, 2, 3, -1, -2, -3);

        IntStream negatives =
                ints.filter(el -> el < 0); // lazy

        negatives.forEach(System.out::println); // terminated
    }
}
