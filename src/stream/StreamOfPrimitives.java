package stream;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamOfPrimitives {
    public static void main(String[] args) {
        // int
        IntStream intsOpen = IntStream.range(1, 100); // 1-99
        IntStream intsClosed = IntStream.rangeClosed(1, 100); // 1-100
        // char
        IntStream chars = "sentence/".chars(); // now CharStream
        chars.mapToObj(el -> (char)el)
             .forEach(System.out::print);
        // or..
        Stream<Character> charStream = "sentence/".chars().mapToObj(el -> (char)el);
        charStream.forEach(System.out::print);
    }
}
