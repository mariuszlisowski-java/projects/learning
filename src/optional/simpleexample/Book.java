package optional.simpleexample;

import java.util.UUID;

public class Book {
    UUID uuid;
    String title;

    public Book(String title) {
        this.uuid = UUID.randomUUID();
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Book{" +
                "uuid=" + uuid +
                ", title='" + title + '\'' +
                '}';
    }
}
