package optional.simpleexample;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BasicExample {
    private List<Book> books;

    public BasicExample() {
        init();
        Optional<Book> book = books
                .stream()
                .filter(element -> element.getTitle().equals("BookA"))
                .findFirst();

        // out
        book.ifPresentOrElse(element -> System.out.println(element.getTitle()),
                             () -> System.out.println("No such title"));
        // throw
        Book foundBook = book.orElseThrow(() -> new RuntimeException("No such title"));
        System.out.println(foundBook);
    }

    private void init() {
        books = new ArrayList<>();
        books.add(new Book("BookA"));
        books.add(new Book("BookB"));
        books.add(new Book("BookC"));
    }

    public static void main(String[] args) {
        new BasicExample();
    }
}
