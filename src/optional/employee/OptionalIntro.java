package optional.employee;

import optional.employee.Employee;
import optional.employee.EmployeeRepository;

import java.util.Optional;
import java.util.function.Consumer;

public class OptionalIntro {
    public static void main(String[] args) {
        Optional<Employee> employee = EmployeeRepository.find("Alice");
        Optional<Employee> nonExist = EmployeeRepository.find("None");

        // always check if not empty
        if (employee.isPresent()) {
            Employee emp = employee.get(); // if empty then throws NoSuchElementException
            System.out.println(emp.getName());
        }
        // shorter implementation
        employee.ifPresent(new Consumer<Employee>() {
            @Override
            public void accept(Employee employee) {
                System.out.println(employee.getName());
            }
        });
        // shortest implementation
        employee.ifPresent(employee1 -> System.out.println(employee.get().getName()));

        // another implementation (stream)
        employee.map(Employee::getName)             // returns Optional<String>
                .ifPresent(System.out::println);
        // conditional filter
        employee.filter(emp -> emp.getAge() > 20) // only employees older than ...
                .map(Employee::getName)
                .map(String::toUpperCase)
                .ifPresent(System.out::println);
        // conditional else
        String personA = employee.filter(emp -> emp.getAge() < 20)
                                 .map(Employee::getName)
                                 .orElse("Nobody that young");
        System.out.println(personA);
        // exception
        try {
            String personB = employee.filter(emp -> emp.getAge() < 20)
                    .map(Employee::getName)
                    .orElseThrow(() -> new RuntimeException("Nobody that young exception"));
            System.out.println(personB);
        } catch (RuntimeException exception) {
            System.out.println(exception.getMessage());
        }
        // not found so created
        nonExist.or( () -> Optional.of(new Employee("Mark", 18)) )
                .filter(emp -> emp.getAge() < 20)
                .map(Employee::getName)
                .map(String::toUpperCase)
                .ifPresent(System.out::println);
        // not found so created but did not pass the filter
        nonExist.or( () -> Optional.of(new Employee("Mark", 21)) )
                .filter(emp -> emp.getAge() < 20)
                .map(Employee::getName)         // empty Optional<String>
                .map(String::toUpperCase)       // no action
                .ifPresentOrElse(
                        System.out::println,                                    // if present
                        () -> System.out.println("Nobody that young, sorry")    // if empty
                );
    }
}
