package optional.employee;

import optional.employee.Employee;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class EmployeeRepository {
    private static Map<String, Employee> employees = new HashMap<>(2);

    static {
        employees.put("Alice", new Employee("Alice", 22));
        employees.put("Betty", new Employee("Betty", 32));
    }

    public static Optional<Employee> find(String name) {
        // Optional.empty(); // always return an empty optional
        // Optional.of(null); // wraps a null and returns optional with null value
        return Optional.ofNullable(employees.get(name)); // if null passed returns empty optional
    }
}
