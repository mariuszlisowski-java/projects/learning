package oop.interfaces.defaults;

public class Main {
    public static void main(String[] args) {
        Car transit = new Van();
        Car isuzu = new Truck();

        transit.carryLoad();
        transit.gas();
        transit.brake();

        isuzu.carryLoad();
        isuzu.gas();
        isuzu.brake();

    }
}
