package oop.interfaces.defaults;

interface Car {
    // no need to override
    default void gas() {
        System.out.println("Accelerating...");
    }

    // no need to override
    default void brake() {
        System.out.println("Stopping...");
    }

    // must be overridden
    void carryLoad();
}
