package oop.interfaces.defaults;

public class Van implements Car {
    @Override
    public void carryLoad() {
        System.out.println("Van: carrying light loads...");
    }
}
