package oop.interfaces.defaults;

public class Truck implements Car {
    // interface method MUST be overridden
    @Override
    public void carryLoad() {
        System.out.println("Truck: carrying heavy loads...");
    }

    // default interface method CAN be overridden
    @Override
    public void gas() {
        System.out.println("Truck stoppling slowly...");
    }
}
