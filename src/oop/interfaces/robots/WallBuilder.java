package oop.interfaces.robots;

interface WallBuilder {
    void buildWall();
}
