package oop.interfaces.robots;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Object> builders = new ArrayList<>();
        builders.add(new BuilderRobot());
        builders.add(new WorkerRobot());
        builders.add(new GuardRobot());

        for (Object o : builders) {
            if (o instanceof WallBuilder) {
                ((WallBuilder) o).buildWall();
            } else if (o instanceof GuardRobot){
                ((GuardRobot) o).doNothing();
            }
        }

    }

}
