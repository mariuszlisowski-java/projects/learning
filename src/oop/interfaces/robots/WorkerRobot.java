package oop.interfaces.robots;

public class WorkerRobot implements WallBuilder {
    @Override
    public void buildWall() {
        System.out.println("Worker builds a wall");
    }
}
