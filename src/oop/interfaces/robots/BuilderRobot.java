package oop.interfaces.robots;

public class BuilderRobot implements WallBuilder {
    @Override
    public void buildWall() {
        System.out.println("Builder builds a wall");
    }
}
