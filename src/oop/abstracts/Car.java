package oop.abstracts;

public abstract class Car {
    private String model;
    private String clolor;

    public abstract void accelerate();
    // empty implementation
    public void brake() {};
}
