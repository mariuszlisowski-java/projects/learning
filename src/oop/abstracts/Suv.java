package oop.abstracts;

public class Suv extends Car {
    // MUST be overridden
    @Override
    public void accelerate() {}

    // MAY be overrriden
    @Override
    public void brake() {}
}
