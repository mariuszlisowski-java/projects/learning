package oop.instance;

public class Truck extends Car {
    Integer capacity;

    public Truck(String model, Integer capacity) {
        super(model);
        this.capacity = capacity;
    }

    public static void main(String[] args) {
        Truck truck = new Truck("Pick up", 5200);
        Car car = new Car("A car");

        System.out.println(isInstanceOfCar(truck)); // all trucks are cars
        System.out.println(isInstanceOfCar(car));   // not all cars are trucks
    }

    // instanceof operator usage
    private static boolean isInstanceOfCar(Truck truck) {
        return truck instanceof Car;
    }
    private static boolean isInstanceOfCar(Car car) {
        return car instanceof Truck;
    }

}
