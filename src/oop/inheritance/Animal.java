package oop.inheritance;

public class Animal {
    String brain;
    String heart;

    public Animal() {}
    public Animal(String brain, String heart) {
        this.brain = brain;
        this.heart = heart;
        System.out.println("Zoo.Animal constructor");
    }
}
