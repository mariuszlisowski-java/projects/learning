package oop.inheritance;

public class Cat extends Animal {
    String tail;

    public Cat(String brain, String heart, String tail) {
        super(brain, heart);
        this.tail = tail;
        System.out.println("Cat constructor");
    }

}
