package functional.something;

public class Test {
    public static void main(String[] args) {
        // full implementation of interface
        Something hiMessage = new Something() {
            @Override
            public void doSomething() {
                printMessage("Hi!");
            }
        };
        hiMessage.doSomething();

        // lambda (short)
        Something helloMessage = () -> printMessage("Hello");
        helloMessage.doSomething();

        // method reference
        Something surprise = Test::printMsg; // for no arguments
        surprise.doSomething();
    }

    private static void printMessage(String message) {
        System.out.println(message);
    }

    private static void printMsg() {
        System.out.println("Suprise!");
    }

}
