package functional.something;

@FunctionalInterface
public interface Something {
    public void doSomething();
}
