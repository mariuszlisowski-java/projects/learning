package functional.calculations;

public class Main {
    public static void main(String[] args) {
        // sum
        Calculation sumA = new Calculation() {
            @Override
            public int calculate(int a, int b) {
                return a + b;
            }
        };
        Calculation sumB = (a, b) -> a + b;
        Calculation sumC = Integer::sum;

        int resultA = sumA.calculate(3, 2);
        int resultB = sumB.calculate(3, 2);
        int resultC = sumC.calculate(3, 2);
        System.out.println(resultA + " " + resultB + " " + resultC);

        // subtract
        Calculation subtract = (a, b) -> a - b;
        System.out.println(subtract.calculate(3, 2));

        // divide
        Calculation divide = (a, b) -> {
            if (b != 0) {
                return a / b;
            } else {
                throw new ArithmeticException("Divide by zero!");
            }
        };
        try {
            System.out.println(divide.calculate(3, 0));
        } catch (ArithmeticException exception) {
            System.out.println(exception.getMessage());
        }

    }
}
