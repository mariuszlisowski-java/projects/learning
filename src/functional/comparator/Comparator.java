package functional.comparator;

// annotation checks if interface is functional
// i.e. if contains only one method
@FunctionalInterface
public interface Comparator<T> {
    // the only method to implement
    int compare(T o1, T o2);

    // object method (not counted here)
    boolean equals(Object obj);

    // default method (implemented)
    default Comparator<T> reversed() {
        return (o1, o2) -> compare(o2, o1);
    }
}
