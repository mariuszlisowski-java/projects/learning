package file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class FileWritePrintWriter {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("c:\\file");

        if (file.canRead()) {
            if (file.length() != 0) {
                System.out.print("File not empty. Overwrite? (y/n): ");
                Scanner scanner = new Scanner(System.in);
                if (scanner.nextLine().equals("y")) {
                    writeToFile(file);
                } else {
                    System.out.println("Nothing written");
                }
            } else {
                writeToFile(file);
            }
        } else {
            System.out.println("File open error!");
        }
    }

    public static void writeToFile(File file) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter(file);
        writer.println("line");
        writer.close();
        System.out.println("Success!");
    }

}
