package file.data.classes;

import java.io.*;

public class ClassStaticWriterReader {

    public static class ClassWithStatic {
        public static String staticString = "To jest statyczny ciąg testowy";
        public int i;
        public int j;

        // write class state
        public void save(OutputStream outputStream) throws Exception {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
            writer.write(staticString + "\n");
            writer.write(i + "\n");
            writer.write(j + "\n");
            writer.close();
        }

        // read class state
        public void load(InputStream inputStream) throws Exception {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            staticString = reader.readLine();
            i = Integer.parseInt(reader.readLine());
            j = Integer.parseInt(reader.readLine());
            reader.close();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ClassWithStatic that = (ClassWithStatic) o;

            if (i != that.i) return false;
            return j == that.j;
        }

        @Override
        public int hashCode() {
            int result = i;
            result = 31 * result + j;
            return result;
        }
    } // ClassWithStatic

    public static void main(String[] args) {
        try {
            // File yourFile = File.createTempFile("test", null);
            File yourFile = new File("znaczki");

            OutputStream outputStream = new FileOutputStream(yourFile);
            InputStream inputStream = new FileInputStream(yourFile);

            ClassWithStatic classWithStatic = new ClassWithStatic();
            classWithStatic.i = 3;
            classWithStatic.j = 4;
            classWithStatic.save(outputStream);
            outputStream.flush();

            ClassWithStatic loadedObject = new ClassWithStatic();
            ClassWithStatic.staticString = "something";
            loadedObject.i = 6;
            loadedObject.j = 7;

            loadedObject.load(inputStream);

            // compare saved & loaded objects
            System.out.println(classWithStatic.equals(loadedObject) ? "same" : "different");

            outputStream.close();
            inputStream.close();

        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("File save/open error");
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Method save/load error");
        }
    } // main

}
