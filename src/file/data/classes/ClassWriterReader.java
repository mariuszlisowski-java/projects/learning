package file.data.classes;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClassWriterReader {

    public static class Human {
        public String name;
        public List<Asset> assets = new ArrayList<>();

        public Human() {}
        public Human(String name, Asset... assets) {
            this.name = name;
            if (assets != null) {
                this.assets.addAll(Arrays.asList(assets));
            }
        }

        // write class state
        public void save(OutputStream outputStream) throws Exception {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
            String isNameEmpty = name.isEmpty() ? "yes" : "no";
            String isAssetsListEmpty = assets.isEmpty() ? "yes" : "no";

            writer.write(isNameEmpty + "\n");
            writer.write(isAssetsListEmpty + "\n");

            if (!name.isEmpty()) {
                writer.write(name + "\n");
            }
            if (!assets.isEmpty()) {
                writer.write(assets.size() + "\n");
                for (Asset asset : assets) {
                    writer.write(asset.getName() + "\n");
                    writer.write(asset.getPrice() + "\n");
                }
            }
            writer.close();
        }

        // read class state
        public void load(InputStream inputStream) throws Exception {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String isNameEmpty = reader.readLine();
            String isAssetsListEmpty = reader.readLine();

            if (isNameEmpty.equals("no")) {
                this.name = reader.readLine();
            }
            if (isAssetsListEmpty.equals("no")) {
                int assetsCount = Integer.parseInt(reader.readLine());
                for (int i = 0; i < assetsCount; i++) {
                    assets.add(new Asset(reader.readLine(), Double.parseDouble(reader.readLine())));
                }
            }
            reader.close();
        }
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Human human = (Human) o;

            if (name != null ? !name.equals(human.name) : human.name != null) return false;
            return assets != null ? assets.equals(human.assets) : human.assets == null;
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (assets != null ? assets.hashCode() : 0);
            return result;
        }

    } // human

    public static void main(String[] args) {
        try {
            File yourFile = File.createTempFile("znaczki", null);
            // File yourFile = new File("test");
            OutputStream outputStream = new FileOutputStream(yourFile);
            InputStream inputStream = new FileInputStream(yourFile);

            Human smith = new Human("Smith", new Asset("house", 999_999.99),
                    new Asset("car", 2999.99));
            // Human smith = new Human("Smith", null);
            smith.save(outputStream);
            outputStream.flush();
            outputStream.close();

            Human somePerson = new Human();
            somePerson.load(inputStream);
            inputStream.close();

            // compare saved & loaded objects
            System.out.println(smith.equals(somePerson) ? "Same persons" : "Different persons");

        } catch (IOException e) {
            // e.printStackTrace();
            System.out.println("File save/open error");
        } catch (Exception e) {
            // e.printStackTrace();
            System.out.println("Method save/load error");
        }
    } // main

    public static class Asset {
        public Asset(String name, double price) {
            this.name = name;
            this.price = price;
        }

        private String name;
        private double price;

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Asset asset = (Asset) o;

            if (Double.compare(asset.price, price) != 0) return false;
            return name != null ? name.equals(asset.name) : asset.name == null;
        }

        @Override
        public int hashCode() {
            int result;
            long temp;
            result = name != null ? name.hashCode() : 0;
            temp = Double.doubleToLongBits(price);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }
    } // asset

}
