package file.data.stream.buffered;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

public class DisplayFileBuffered {

    public static void main(String[] args) {
        // argument passed ?
        if (args.length > 0) {
            displayFile(args[0]);   // assume first argument is a filename
        } else {
            System.out.println("Usage: cat <filename>");
        }
    }

    // optimized (buffered with set buffer size)
    private static void displayFile(String filename) {
        Date startTime = new Date(); // measure displaying time

        try (InputStream inputStream = new FileInputStream(filename);
             BufferedInputStream bufferedInput = new BufferedInputStream(inputStream, 200)) {

            int byteValue;
            while ((byteValue = bufferedInput.read()) != -1) {
                System.out.print((char) byteValue);
            }
        } catch (IOException e) {
            System.out.println("No such file! Check path and/or filename...");
        }

        Date endTime = new Date();
        System.out.println("Displaying time: " + (endTime.getTime() - startTime.getTime()));
    }

}
