package file.data.stream.buffered;

import java.io.*;

public class CopyFileBufferedAppend {

    public static void main(String[] args) {
        String filenameInput = "test.text";
        String filenameOutput = "output";

        // optimized by buffered input/output & buffer usage
        try (InputStream inputStream = new FileInputStream(filenameInput);
             BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
             OutputStream outputStream = new FileOutputStream(filenameOutput, true);
             BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream))
        {
            int bytesRead;
            byte[] buffer = new byte[10000];    // does it help to optimize here?
            while ((bytesRead = bufferedInputStream.read(buffer)) != -1) {
                bufferedOutputStream.write(buffer,0, bytesRead);
            }
            System.out.println("Successfully appended");
        } catch (IOException e) {
            System.out.println("IO error!");
        }


    }


}
