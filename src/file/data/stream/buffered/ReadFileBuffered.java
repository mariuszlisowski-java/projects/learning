package file.data.stream.buffered;

import java.io.*;

public class ReadFileBuffered {
    public static void main(String[] args) {
        InputStream input = null;
        BufferedInputStream buffer = null;

        try {
            // initialize variables
            String filename = "test.text";
            input = new FileInputStream(filename);
            buffer = new BufferedInputStream(input);
            // read byte
            while (buffer.available() > 0) {
                System.out.print((char)buffer.read()); // has to be casted
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // release resources
            try {
                if (input != null) {
                    input.close();
                }
                if (buffer != null) {
                    buffer.close();
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }

        }

    }

}
