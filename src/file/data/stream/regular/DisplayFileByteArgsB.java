package file.data.stream.regular;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DisplayFileByteArgsB {
    public static void main(String args[]) {
        int i;
        FileInputStream fin = null;

        if(args.length != 1) {
            System.out.println("Usage: show filename");
            return;
        }

        try {
            fin = new FileInputStream(args[0]);

            do {
                i = fin.read();
                if(i != -1) System.out.print((char) i);
            } while(i != -1);

        } catch(FileNotFoundException exc) {
            System.out.println("File not found!");
        } catch(IOException exc) {
            System.out.println("IO error!");
        } finally {
            try {
                if(fin != null) fin.close();
            } catch(IOException exc) {
                System.out.println("File close error!");
            }
        } // try

    }

}
