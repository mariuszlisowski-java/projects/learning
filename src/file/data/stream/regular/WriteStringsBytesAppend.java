package file.data.stream.regular;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class WriteStringsBytesAppend {

    public static void main(String[] args) {

        writeString("Ala ma kota", "kot.text");
        writeString("A Ela nie ma", "kot.text");
    }

    private static void writeString(String str, String filename) {
        try (OutputStream outputStream = new FileOutputStream(filename, true)) {
            String line = str.concat("\n");
            outputStream.write(line.getBytes());            // returns byte[] array
            System.out.println("Text written to file");
        } catch (IOException e) {
            System.out.println("File write error!");
        }
    }

}
