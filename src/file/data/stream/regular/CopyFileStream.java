package file.data.stream.regular;

import javax.swing.*;
import java.io.*;

public class CopyFileStream {
    public static String INPUT_FILE = "";
    public static String OUTPUT_FILE = "";

    public static void main(String[] args) {
        try {
            fileCopy(INPUT_FILE, OUTPUT_FILE);
        } catch (IOException exception) {
            System.out.println("File copy error!");
        }
    }

    public static void fileCopy(String inputFile, String outputFile) throws IOException {
        InputStream fileInputStream = null;
        OutputStream fileOutputStream = null;
        try {
            fileInputStream = new FileInputStream(inputFile);
            fileOutputStream = new FileOutputStream(outputFile);
            while (fileInputStream.available() > 0) {
                int dane = fileInputStream.read();
                fileOutputStream.write(dane);
            }
        } catch (FileNotFoundException exception) {
            System.out.println("File not found!");
        } catch (IOException exception) {
            exception.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                fileInputStream.close();
            }
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
        }
    }

}
