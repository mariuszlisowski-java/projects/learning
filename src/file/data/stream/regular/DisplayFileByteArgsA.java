package file.data.stream.regular;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

public class DisplayFileByteArgsA {

    public static void main(String[] args) {
        // argument passed ?
        if (args.length > 0) {
            displayFile(args[0]);   // assume first argument is a filename
        } else {
            System.out.println("Usage: cat <filename>");
        }
    }

    // not optimized (read byte by byte)
    private static void displayFile(String filename) {
        Date startTime = new Date(); // measure displaying time

        try (InputStream inputStream = new FileInputStream(filename)) {

            int byteValue;
            while ((byteValue = inputStream.read()) != -1) {
                System.out.print((char) byteValue);
            }
        } catch (IOException e) {
            System.out.println("No such file! Check path and/or filename...");
        }

        Date endTime = new Date();
        System.out.println("Displaying time: " + (endTime.getTime() - startTime.getTime()));
    }

}
