package file.data.stream.regular;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyFileStreamByteAvail {
    public static void main(String[] args) {
        String filenameIn = "test.data";
        String filenameOut = "test.copy";

        try {
            fileCopy(filenameIn, filenameOut);
            System.out.println("File copied.");
        } catch (IOException e) {
            System.out.println("Error copying file!");
        }
    }

    public static void fileCopy(String inputFilename, String outputFilename) throws IOException {
        FileInputStream inputStream = new FileInputStream(inputFilename);
        FileOutputStream outputStream = new FileOutputStream(outputFilename);

        while (inputStream.available() > 0) {
            outputStream.write(inputStream.read());
        }

        inputStream.close();
        outputStream.close();
    }

}