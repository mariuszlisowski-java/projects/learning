package file.data.stream.regular;

import java.io.*;

public class CopyFileBufferAll {
    public static void main(String[] args) throws IOException {
        String inputFilename = "test.text";
        String outputFilename = "test.txt";

        copyFileAsWhole(inputFilename, outputFilename);
    }

    private static void copyFileAsWhole(String input, String output) throws IOException {
        InputStream inputStream = new FileInputStream(input);
        OutputStream outputStream = new FileOutputStream(output);

        // copy using one block
        if (inputStream.available() > 0 ) {
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            outputStream.write(buffer);
        }

        inputStream.close();
        outputStream.close();
    }

}
