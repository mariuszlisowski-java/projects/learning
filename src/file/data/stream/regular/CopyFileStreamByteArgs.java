package file.data.stream.regular;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyFileStreamByteArgs {
    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            System.out.println("Usage: copy source destination");
            return;
        }

        try (FileInputStream fin = new FileInputStream(args[0]);
             FileOutputStream fout = new FileOutputStream(args[1]))
        {
            int value;
             do {
                value = fin.read();
                if (value != -1) {
                    fout.write(value);
                }
             } while (value != -1);
        } catch (IOException ex) {
            System.out.println("IO error!");
        }
    }

}
