package file.data.stream.regular;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyFileStreamByte {
    public static void main(String[] args) {
        String filenameIn = "test.data";
        String filenameOut = "test.copy";

        fileCopy(filenameIn, filenameOut);
        System.out.println(fileCopy(filenameIn, filenameOut) ?
                "File copied" : "Error copying file!");
    }

    public static boolean fileCopy(String inputFilename, String outputFilename) {
        try (FileInputStream fin = new FileInputStream(inputFilename);
             FileOutputStream fout = new FileOutputStream(outputFilename))
        {
            int value;
            do {
                value = fin.read();
                if (value != -1) {
                    fout.write(value);
                }
            } while (value != -1);
            return true;
        } catch (IOException ex) {
            System.out.println("IO error!");
            return false;
        }
    }

}
