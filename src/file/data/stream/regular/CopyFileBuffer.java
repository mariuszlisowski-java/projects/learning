package file.data.stream.regular;

import java.io.*;

public class CopyFileBuffer {

    public static void main(String[] args) throws IOException {
        String inputFilename = "test.text";
        String outputFilename = "test.txt";

        copyFileInBlocks(inputFilename, outputFilename);
    }

    private static void copyFileInBlocks(String input, String output) throws IOException {
        InputStream inputStream = new FileInputStream(input);
        OutputStream outputStream = new FileOutputStream(output);

        // copy in blocks (last block can be smaller)
        byte[] buffer = new byte[1000];
        while (inputStream.available() > 0) {
            int length = inputStream.read(buffer);       // length read
            outputStream.write(buffer, 0, length);  // buffer, offset, length
        }

        inputStream.close();
        outputStream.close();
    }

}
