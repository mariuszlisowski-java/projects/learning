package file.data.stream.data;

import java.io.*;

public class ReadWriteData {
    public static void main(String args[]) {
        int i = 9;
        double d = 3.14;
        boolean b = true;
        // writing
        try (DataOutputStream dataOut =
                     new DataOutputStream(new FileOutputStream("test.data"))) {
            dataOut.writeInt(i);
            dataOut.writeDouble(d);
            dataOut.writeBoolean(b);
        } catch (IOException exc) {
            System.out.println("Write error!");
            return;
        }
        // reading
        try (DataInputStream dataIn =
                     new DataInputStream(new FileInputStream("test.data"))) {
            i = dataIn.readInt();
            d = dataIn.readDouble();
            b = dataIn.readBoolean();
        } catch (IOException exc) {
            System.out.println("Read error!");
        }
        System.out.println(i + " " + d + " " + b);
    }
}
