package file.text.buffered;

import java.io.*;

public class CopyFileString {
    public static void main(String[] args) throws IOException {
        String filenameIn = "test.text";
        String filenameOut = "test.copy";

        fileCopy(filenameIn, filenameOut);
        System.out.println(fileCopy(filenameIn, filenameOut) ?
                           "File copied" : "Error copying file!");
    }

    public static boolean fileCopy(String inputFilename, String outputFilename) throws IOException {
        try (BufferedReader fileReader = new BufferedReader(new FileReader(inputFilename));
             BufferedWriter fileWriter = new BufferedWriter(new FileWriter(outputFilename)))
        {
            String line;
            while((line = fileReader.readLine()) != null){
                    fileWriter.write(line);
                    fileWriter.newLine();
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}
