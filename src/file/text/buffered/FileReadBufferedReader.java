package file.text.buffered;

import java.io.*;

public class FileReadBufferedReader {
    public static void main(String[] args) {
        InputStream input = null;
        InputStreamReader stream = null;
        BufferedReader buffer = null;

        try {
            // initialize variables
            String filename = "test.text";
            input = new FileInputStream(filename);
            stream = new InputStreamReader(input);
            buffer = new BufferedReader(stream);
            String line = null;
           // read line
            while ((line = buffer.readLine()) != null) {
                System.out.println(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // release resources
            try {
                if (input != null) {
                    input.close();
                }
                if (stream != null) {
                    stream.close();
                }
                if (buffer != null) {
                    buffer.close();
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }

        }
    }


}
