package file.text.buffered;

import java.io.*;

public class FileWriteBufferedWriter {

    public static void main(String[] args) throws IOException {
        String filename = "test.lines";
        // console reader init
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        // file writer init
        OutputStream output = new FileOutputStream(filename);
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));

        String line;
        do {
            System.out.print(": ");
            line = reader.readLine();
            writer.write(line + '\n');
        } while (!(line.equals("exit")));

        reader.close();
        writer.close(); // closed as first
        output.close(); // closed as second
    }

}
