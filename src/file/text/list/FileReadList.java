package file.text.list;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileReadList {

    public static void main(String[] args) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get("test.text"));

        lines.stream().forEach(System.out::println);
    }
}
