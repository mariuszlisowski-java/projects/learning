package file.text.regular;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CopyFileChar {

    public static void main(String[] args) throws IOException {
        FileReader reader = new FileReader("test.text");
        FileWriter writer = new FileWriter("out.text");

        while (reader.ready()) {
            int data = reader.read();   // read char
            writer.write(data);         // write char
        }

        reader.close();
        writer.close();
    }

}
