package file;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class FileWritePrintStream {
    private static final String SENTENCE = "%d. %s ma kota o imieniu %s\n";

    public static void main(String[] args) {
        try (PrintStream filePrintStream = new PrintStream("file.txt")) {

            filePrintStream.printf(SENTENCE, 1, "Ala", "Kotek");
            filePrintStream.printf(SENTENCE, 2, "Ela", "Kocur");

            System.out.println("File successfully written as follows:");
            System.out.printf(SENTENCE, 1, "Ala", "Kotek");
            System.out.printf(SENTENCE, 2, "Ela", "Kocur");

        } catch (FileNotFoundException e) {
            System.out.println("File write error!");
        }
    }

}
