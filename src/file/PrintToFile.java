package file;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class PrintToFile {
    public static void main(String arr[]) throws FileNotFoundException, FileNotFoundException {
        PrintStream filePrintStream = new PrintStream("out.text");

        // save default output
        PrintStream console = System.out;

        // use file output
        System.setOut(filePrintStream);
        System.out.println("This line will be written to the text file");

        // restore default output
        System.setOut(console);
        System.out.println("But this line will be output to the console!");
    }
}
