package file;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReadScanner {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("c:\\file");
        if (file.canRead()) {
            if (file.length() > 0) {
                Scanner scanner = new Scanner(file);
                System.out.println("Bytes to read: " + file.length());
                System.out.println("Reading...");
                while (scanner.hasNextLine()) {
                    String text = scanner.nextLine();
                    System.out.println(text);
                }
            } else {
                System.out.println("File is empty!");
            }
        } else {
            System.out.println("File read error!");
        }
    }

}
