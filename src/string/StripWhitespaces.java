package string;

public class StripWhitespaces {
    public static void main(String[] args) {
        String str = "  Hello World !!   ";

        replaceAllWhitespaces11(str);
        replaceAllWhitespaces(str);
    }

    public static void replaceAllWhitespaces11(String str) {
        System.out.println("\"" + str.strip() + "\"");          // "Hello World !!"
        System.out.println("\"" + str.stripLeading() + "\"");   // "Hello World !!   "
        System.out.println("\"" + str.stripTrailing() + "\"");  // "  Hello World !!"
        System.out.println();
    }

    public static void replaceAllWhitespaces(String str) {
        System.out.println( str.replaceAll("^[ \t]+|[ \t]+$", "") ); // "Hello World !!"
        System.out.println( str.replaceAll("^[ \t]+", "") );         // "Hello World !!   "
        System.out.println( str.replaceAll("[ \t]+$", "") );         // "  Hello World !!"
        System.out.println();
    }

}
