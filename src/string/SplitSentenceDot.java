package string;

public class SplitSentenceDot {
    public static void main(String[] args) {
        String[] words = splitSentenceByDot("Ala.ma.kota");

        for (String word : words) {
            System.out.println("'" + word + "'");
        }
    }

    public static String[] splitSentenceByDot(String sentence) {
        String[] words = sentence.split("\\.");

        return words;
    }

}
