package string;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

public class StringDistinct {
    public static void main(String[] args) {
        String[] duplicates = {"ola", "nie", "ala", "ma", "ala", "kota", "burka", "burka", "ola"};

        String[] uniques = removeDuplicatesUnordered(duplicates);
//        String[] uniques = removeDuplicatesOrderedShort(duplicates);

        System.out.println(Arrays.asList(duplicates));
        System.out.println(Arrays.asList(uniques));

    }

    public static String[] removeDuplicatesOrderedShort(String[] array) {
        return new LinkedHashSet<String>(Arrays.asList(array)).toArray(String[]::new);
    }

    public static String[] removeDuplicatesOrdered(String[] array) {
        Set<String> temp = new LinkedHashSet<String>(Arrays.asList(array));
        String[] result = temp.toArray(String[]::new);

        return result;
    }

    // must be sorted inside
    public static String[] removeDuplicatesUnordered(String[] array) {
        return Arrays.stream(array).sorted().distinct().toArray(String[]::new);
    }

}
