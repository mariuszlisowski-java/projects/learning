package string;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReverseStrings {
    private static List<String> strings = new ArrayList<>();

    static {
        strings.add("ela");
        strings.add("ma");
        strings.add("kota");
    }

    public static void main(String[] args) throws IOException {
        reverseEachLine(strings);
        printList(strings);
    }

    private static void printList(List<String> content) {
        content.stream()
                .forEach(System.out::println);
    }

    // reverse in place (reference used)
    private static void reverseEachLine(List<String> list) {
        int index = 0;
        for (String string : list) {
            list.set(index++, new StringBuilder(string).reverse().toString());
        }
    }
}
