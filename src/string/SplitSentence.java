package string;

public class SplitSentence {
    public static void main(String[] args) {
        String[] words = splitSentence("Ala ma kota");

        for (String word : words) {
            System.out.println("'" + word + "'");
        }
    }

    public static String[] splitSentence(String sentence) {
        String[] words = sentence.split(" ");

        return words;
    }
}
