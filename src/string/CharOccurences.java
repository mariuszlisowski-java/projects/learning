package string;

public class CharOccurences {
    public static void main(String[] args) {
        long occurences = charOccurencesCounter("Ala ma kota", 'a');

        System.out.println(occurences);
    }

    public static long charOccurencesCounter(String sentence, char charToCount) {
        long charOccurences = sentence.chars().filter(ch -> ch == charToCount).count();

        return charOccurences;
    }
}
