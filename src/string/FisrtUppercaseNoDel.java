package string;

import java.io.IOException;

public class FisrtUppercaseNoDel {
    public static void main(String[] args) throws IOException {
        String s = "o  ala  ma  kota   i   rybki   o ";

        StringBuilder output = new StringBuilder();
        boolean isLetter = true;

        for (char ch : s.toCharArray()) {
            if (ch != ' ' && isLetter) {
                output.append(Character.toUpperCase(ch));
            } else {
                output.append(ch);
            }
            isLetter = (ch == ' ');
        }

        System.out.println(output);
    }

}
