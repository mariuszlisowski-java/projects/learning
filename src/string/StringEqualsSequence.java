package string;

public class StringEqualsSequence {
    public static void main(String[] args) {
        String str = "Emily";           // cannot be null
        String toBeComparedStr = null;  // can be null

        // no exception
        if (str != null & str.equals(toBeComparedStr)) { // correct sequence of equals
            System.out.println("Not the same!");
        }

        // null pointer exception
        if (str != null & toBeComparedStr.equals(str)) { // inclorrect sequence
            System.out.println("Not the same!");
        }

    }

    //        productList.removeIf(el -> el.name.equals(name));

}
