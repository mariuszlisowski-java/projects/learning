package string;

import java.io.IOException;

public class FirstUppercaseDelSpaces {
    public static void main(String[] args) throws IOException {
        String s = "  o  ala  ma  kota   i   rybki   o ";
        System.out.println("\"" + s + "\"");

        // delete first multiple spaces
        if (s.charAt(0) == ' ') {
            s = s.replaceFirst("[ ]+", "");
        }
        // or
        s = s.stripLeading();
        System.out.println("\"" + s + "\"");

        // split & delete multiple spaces between
        String[] words = s.split("\\s+");

        StringBuilder output = new StringBuilder();
        for (String word : words) {
            // split each word to char array
            char[] chars = word.toCharArray();
            // uppercase first letter only
            chars[0] = Character.toUpperCase(chars[0]);
            // convert char array to string
            word = String.valueOf(chars);
            // append a string with single space
            output.append(word).append(" ");
        }

        // output
        for (String word : words) {
            System.out.println("\"" + word + "\"");
        }
        System.out.println("\"" + output + "\"");
    }

}
