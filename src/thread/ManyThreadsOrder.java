package thread;

public class ManyThreadsOrder extends Thread {
    @Override
    public void run() {
        System.out.println("Thread executed: " + getName());
    }

    // no order guaranteed
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            ManyThreadsOrder thread = new ManyThreadsOrder();
            thread.start();
        }
    }

}
