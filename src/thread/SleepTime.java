package thread;

public class SleepTime extends Thread {

    public static void main(String[] args) throws InterruptedException {
        SleepTime sleepTime = new SleepTime();

        long startTime = System.currentTimeMillis();
        Thread.sleep(2500);
        long stopTime = System.currentTimeMillis();

        sleepTime.interrupt();
        System.out.println("- How long did I sleep? \n- " + (stopTime - startTime) / (1000 * 1.0) + " secs");
    }

    public SleepTime() {
        this.start();
    }

    public void run() {
        System.out.println("Thread started...");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.print('.');
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                System.out.println("\n# Interrupted !");
                Thread.currentThread().interrupt();
            }
        }
    }

}
