package thread.clock;

import java.util.concurrent.TimeUnit;

public class TickTock {
    State state;

    synchronized void tick(Control control) throws InterruptedException {
        // stop the clock
        if (control == Control.STOP) {
            state = State.TICKED;
            notify();

            return;
        }
        // run the clock
        System.out.print("tick ");
        TimeUnit.SECONDS.sleep(1);
        state = State.TICKED;
        // wait for 'tock'
        notify();
        while (state.equals(State.TICKED)) {
            wait();
        }
    }

    synchronized void tock(Control control) throws InterruptedException {
        // stop the clock
        if (control == Control.STOP) {
            state = State.TOCKED;
            notify();

            return;
        }
        // run the clock
        System.out.println("tock");
        TimeUnit.SECONDS.sleep(1);
        state = State.TOCKED;
        // wait for 'tick'
        notify();
        while (state.equals(State.TOCKED)) {
            wait();
        }
    }

    private enum State {
        TICKED, TOCKED
    }
}
