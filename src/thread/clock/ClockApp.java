package thread.clock;

import java.util.concurrent.TimeUnit;

public class ClockApp {
    public static void main(String[] args) throws InterruptedException {
        TickTock ticktock = new TickTock();
        Clock clockTick = Clock.createAndStart("tick", ticktock);
        Clock clockTock = Clock.createAndStart("tock", ticktock);

        TimeUnit.SECONDS.sleep(6); // let the clock work
        clockTick.stop();
        clockTock.stop();

        clockTick.thread.join();
        clockTock.thread.join();

    }
}
