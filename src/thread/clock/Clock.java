package thread.clock;

public class Clock implements Runnable {
    Thread thread;
    TickTock ticktock;
    private Control control;

    private Clock(String name, TickTock ticktock) {
        this.thread = new Thread(this, name);
        this.ticktock = ticktock;
        this.control = Control.START;
    }

    public static Clock createAndStart(String name, TickTock ticktock) {
        Clock clock = new Clock(name, ticktock);
        clock.thread.start();

        return clock;
    }

    public void stop() {
        control = Control.STOP;
    }

    @Override
    public void run() {
        if (thread.getName().compareTo("tick") == 0) {
            try {
                while (control.equals(Control.START)) {
                    ticktock.tick(Control.START);
                }
                ticktock.tick(Control.STOP);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            try {
                while (control.equals(Control.START)) {
                    ticktock.tock(Control.START);
                }
                ticktock.tock(Control.STOP);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
