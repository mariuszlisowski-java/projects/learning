package thread.runnable.simpleusage;

public class SleeperThread implements Runnable {
    Thread thread;

    SleeperThread(String threadName) {
        this.thread = new Thread(this, threadName);
    }

    public static SleeperThread createAndStartThread(String name) {
        SleeperThread sleeperThread = new SleeperThread("Sleeper");
        sleeperThread.thread.start();

        return sleeperThread;
    }

    @Override
    public void run() {
        try {
            System.out.println(thread.getName() + " is running...");
            Thread.sleep(1000); // may throw an exception
        } catch (InterruptedException exception) {
            System.out.println(thread.getName() + " was interrupted");
        }
        System.out.println(thread.getName() + " has finished");
    }
}
