package thread.runnable.simpleusage;

public class UseThreadSimple {
    public static void main(String[] args) {
        // new thread creation and run
        SleeperThread sleeperThread = SleeperThread.createAndStartThread("Sleeper");

        // delay main thread until previous finishes
        try {
            Thread.sleep(1100);
        } catch (InterruptedException exception) {
            System.out.println("Main thread interrupted");
        }
        System.out.println("Main thread has finished");
    }
}
