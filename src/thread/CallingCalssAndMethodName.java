package thread;

public class CallingCalssAndMethodName {
    public static void main(String[] args) {
        log("In main");
    }

    public static void log(String message) {
        String callingClass = Thread.currentThread().getStackTrace()[2].getClassName();
        String callingMethod = Thread.currentThread().getStackTrace()[2].getMethodName();

        System.out.println("\nClass:   " + callingClass +
                           "\nMethod:  " + callingMethod +
                           "\nMessage: " + message);
    }
}
