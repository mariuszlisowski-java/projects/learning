package thread.interrupt;

public class TickTockUnofficial extends Thread {
    private boolean isCancelled = false;

    public TickTockUnofficial(String name) {
        super(name);
        this.start();
    }

    public void run() {
        try {
            while (!isCancelled) {
                System.out.println("tick");
                Thread.sleep(1000);
                System.out.println("tock");
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void cancel() {
        isCancelled = true;
    }

    public static void main(String[] args) throws InterruptedException {
        TickTockUnofficial clock = new TickTockUnofficial("clock");

        Thread.sleep(5000);
        clock.cancel();
    }

}
