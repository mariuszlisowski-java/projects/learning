package thread.interrupt;

public class TickTockFaulty extends Thread {
    public TickTockFaulty(String name) {
        super(name);
        this.start();
    }

    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("tick");
                Thread.sleep(1000);
                System.out.println("tock");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("Interrupted ?");;
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        TickTockFaulty clock = new TickTockFaulty("faulty interruption");

        Thread.sleep(1000);
        clock.interrupt();          // unsuccessful
        Thread.sleep(1000);
        clock.interrupt();          // unsuccessful

        Thread.sleep(1000);
        clock.stop();               // interrupted (deprecated method)
    }

}
