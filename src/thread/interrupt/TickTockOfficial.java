package thread.interrupt;

public class TickTockOfficial extends Thread {
    public TickTockOfficial(String name) {
        super(name);
        this.start();
    }

    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                    System.out.println("tick");
                    Thread.sleep(1000);        // exception occurs here
                    System.out.println("tock");
                    Thread.sleep(1000);        // ... or here
            } catch (InterruptedException e) {       // clears the interrupt status of the thread
                System.out.println("Interrupted!");;
                Thread.currentThread().interrupt();  // restores the interrupt status of the thread
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        TickTockOfficial clock = new TickTockOfficial("official interrupt");

        Thread.sleep(1000);
        clock.interrupt();
    }

}
