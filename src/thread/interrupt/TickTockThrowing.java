package thread.interrupt;

public class TickTockThrowing extends Thread {
    public TickTockThrowing(String name) {
        super(name);
        this.start();
    }

    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("tick");
                Thread.sleep(1000);
                System.out.println("tock");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("Interrupted!");;
                Thread.currentThread().interrupt();
                throw new RuntimeException("Unexpected interrupt", e);  // throw new exception
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        TickTockThrowing clock = new TickTockThrowing("throw new exception");

        Thread.sleep(1000);
        try {
            clock.interrupt();
        } catch (Exception e) {
            System.out.println("Runtime exception caught!");
        }
    }

}
