package thread.simplecreation;

public class SleeperThreadSimple implements Runnable {
    String threadName;

    public SleeperThreadSimple(String threadName) {
        this.threadName = threadName;
    }

    // override runnable interface
    @Override
    public void run() {
        try {
            System.out.println(threadName + " is running...");
            Thread.sleep(1000); // may throw an exception
        } catch (InterruptedException exception) {
            System.out.println(threadName + " was interrupted");
        }
        System.out.println(threadName + " has finished");
    }
}
