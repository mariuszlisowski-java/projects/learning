package thread.simplecreation;

public class UseThread {
    public static void main(String[] args) {
        // new thread creation
        SleeperThreadSimple sleeperThread = new SleeperThreadSimple("Sleeper");
        // and run
        Thread firstThread = new Thread(sleeperThread);
        firstThread.start();

        // delay main thread until previous finishes
        try {
            Thread.sleep(1100);
        } catch (InterruptedException exception) {
            System.out.println("Main thread interrupted");
        }
        System.out.println("Main thread has finished");
    }

}
