package thread.thread;

public class UseThread {
    public static void main(String[] args) {
        ExtendThread thread = new ExtendThread("Thread no. 1");
        thread.start();


        // delay main thread until previous finishes
        try {
            Thread.sleep(1100);
        } catch (InterruptedException exception) {
            System.out.println("Main thread interrupted");
        }
        System.out.println("Main thread has finished");
    }
}
