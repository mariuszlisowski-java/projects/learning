package thread.thread;

public class ExtendThread extends Thread {
    ExtendThread(String name) {
        super(name); // Thread(String threadName)
    }

    public void run() {
        try {
            System.out.println(getName() + " is running..."); // getName() of Thread superclass
            Thread.sleep(1000);                         // may throw an exception
        } catch (InterruptedException exception) {
            System.out.println(getName() + " was interrupted");
        }
        System.out.println(getName() + " has finished");
    }
}
