package thread;

public class CallingMethodName {
    public static void main(String[] args) throws Exception {
        method1();
        System.out.println("\nBack to main method");
    }

    public static void method1() {
        method2();
        displayStackTrace(Thread.currentThread().getStackTrace());
    }

    public static void method2() {
        method3();
        displayStackTrace(Thread.currentThread().getStackTrace());
    }

    public static void method3() {
        displayStackTrace(Thread.currentThread().getStackTrace());
    }

    private  static void displayStackTrace(StackTraceElement[] stackTraceElements) {
        System.out.println("\nCalling methos: " +
                           stackTraceElements[2].getMethodName() + // third element
                           "\nFull stack:");
        for (StackTraceElement element : stackTraceElements) {
            System.out.println("# " + element.getMethodName());
        }
    }
}
