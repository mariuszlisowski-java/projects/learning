package thread.trafficlights;

public enum TraficLightColor {
    RED, GREEN, YELLOW
}
