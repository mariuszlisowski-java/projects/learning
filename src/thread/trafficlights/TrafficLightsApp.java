package thread.trafficlights;

public class TrafficLightsApp {

    public static void main(String[] args) {
        int loops = 8;

        TrafficLightsSimulator trafficLightsSimulator = new TrafficLightsSimulator();

        Thread lights = new Thread(trafficLightsSimulator);
        lights.start();

        while (--loops >= 0)  {
            System.out.println(trafficLightsSimulator.getColor());
            trafficLightsSimulator.waitForChange();
            if (loops % 3 == 0) {
                System.out.println("------");
            }
        }

        trafficLightsSimulator.stop();

    }

}
