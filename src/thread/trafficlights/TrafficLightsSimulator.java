package thread.trafficlights;

public class TrafficLightsSimulator implements Runnable {
    private TraficLightColor color;
    private boolean stop = false;
    private boolean changed = false;

    public  TrafficLightsSimulator() {
        color = TraficLightColor.YELLOW;
    }

    @Override
    public void run() {
        while (stop == false) {
            try {
                switch (color) {
                    case RED:
                        Thread.sleep(3000); break;
                    case GREEN:
                        Thread.sleep(2000); break;
                    case YELLOW:
                        Thread.sleep(500); break;
                }
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
            changeColor();
        }
    }

    private synchronized void changeColor() {
        switch (color) {
            case RED:
                color = TraficLightColor.GREEN; break;
            case GREEN:
                color = TraficLightColor.YELLOW; break;
            case YELLOW:
                color = TraficLightColor.RED; break;
        }
        changed = true;
        notify();
    }

    public synchronized void waitForChange() {
        try {
            while (changed == false) {
                wait();
            }
            changed = false;
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }

    public synchronized TraficLightColor getColor() {
        return color;
    }

    public synchronized void stop() {
        stop = true;
    }
}
