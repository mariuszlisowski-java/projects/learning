package thread.methods.runnablecreate;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        CountThread cThread = CountThread.createAndStart("countdown");

        System.out.println("Waiting for " + cThread.getThrd().getName());
        cThread.getThrd().join();
        System.out.println("Boom!");
    }
}
