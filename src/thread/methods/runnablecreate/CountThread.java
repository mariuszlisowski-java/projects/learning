package thread.methods.runnablecreate;

public class CountThread implements Runnable {
    private Thread thrd;

    public CountThread(String name) {
        this.thrd = new Thread(this, name);
    }

    public Thread getThrd() {
        return thrd;
    }

    public static CountThread createAndStart(String name) {
        CountThread cThread = new CountThread(name); // constructor call (new thread created)
        cThread.thrd.start();

        return cThread;
    }

    @Override
    public void run() throws IllegalArgumentException {
        int counter = 10;
        while (counter >= 0) {
            System.out.print(counter + " ");
            counter--;
            try {
                Thread.sleep(250);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }
    }
}
