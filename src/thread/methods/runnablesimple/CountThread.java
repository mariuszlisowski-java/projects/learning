package thread.methods.runnablesimple;

public class CountThread implements Runnable {
    private String threadName;

    public CountThread(String threadName) {
        this.threadName = threadName;
    }

    public String getThreadName() {
        return threadName;
    }

    @Override
    public void run() {
        int counter = 10;
        while (counter >= 0) {
            System.out.print(counter + " ");
            counter--;
            try {
                Thread.sleep(250);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }
    }
}
