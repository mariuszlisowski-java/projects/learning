package thread.methods.runnablesimple;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        CountThread cThread = new CountThread("countdown");
        Thread thread = new Thread(cThread);
        thread.start();

        System.out.println("Waiting for " + cThread.getThreadName());
        thread.join();
        System.out.println("Boom!");
    }
}
