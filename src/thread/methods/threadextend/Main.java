package thread.methods.threadextend;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        CountThread countThread = new CountThread("countdown");
        countThread.start();

        System.out.println("Waiting for " + countThread.getName());
        countThread.join();
        System.out.println("Boom!");

    }
}
