package thread.methods.threadextend;

public class CountThread extends Thread {
    public CountThread(String name) {
        super(name);
    }

    @Override
    public void run() {
        int counter = 10;
        while (counter >= 0) {
            System.out.print(counter + " ");
            counter--;
            try {
                Thread.sleep(250);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }
    }
}
