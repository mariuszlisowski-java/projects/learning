package thread;

public class CallingLineNumber {
    public static void main(String[] args) throws Exception {
        System.out.println("First method called from line: " + method1());
    }

    public static int method1() {
        System.out.println("Second method called from line: " + method2());
        return  Thread.currentThread().getStackTrace()[2].getLineNumber();
    }

    public static int method2() {
        return  Thread.currentThread().getStackTrace()[2].getLineNumber();
    }

}
