package reflection.animal;

public class Goat extends Animal implements Locomotion {
    @Override
    protected String getSound() {
        return "beee";
    }

    @Override
    public String eats() {
        return "grass";
    }

    @Override
    public String getLocomotion() {
        return "walks";
    }
}
