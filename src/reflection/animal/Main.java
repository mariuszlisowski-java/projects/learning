package reflection.animal;

public class Main {
    public static void main(String[] args) {
        Object goat = new Goat();

        Class<?> clazz = goat.getClass();
        System.out.println(clazz.getName());
        System.out.println(clazz.getSimpleName());
        System.out.println(clazz.getCanonicalName());

    }
}
