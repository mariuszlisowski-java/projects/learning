package algorithm;

import java.math.BigInteger;

public class Factorial {

    public static void main(String[] args) {
        try {
            System.out.println(factorialBelow21(20));
            System.out.println(factorialAbove20(21));
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

    }

    public static String factorialAbove20(int n) {
        BigInteger result;

        if (n == 0 || n == 1) {
            result = BigInteger.valueOf(1);
        } else {
            if (n > 1) {
                result = BigInteger.ONE;
                for (int i = 2; i <= n; i++) {
                    result = result.multiply(BigInteger.valueOf(i));
                }
            } else {
                throw new IllegalArgumentException("Will calculate factorial for positive values");
            }
        }

        return String.valueOf(result);
    }

    public static String factorialBelow21(int n) {
        long factor;

        if (n == 0 || n == 1) {
            factor = 1L;
        } else {
            if (n > 1 && n < 21) {
                factor = 1L;
                for (int i = 2; i <= n; i++) {
                    factor *= i;
                }
            } else {
                throw new IllegalArgumentException("Will calculate factorial from 0 up to 20");
            }
        }

        return String.valueOf(factor);
    }

}