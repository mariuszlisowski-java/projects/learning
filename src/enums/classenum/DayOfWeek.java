package enums.classenum;

public class DayOfWeek {
    private String title;

    private DayOfWeek(String title) {
        this.title = title;
    }

    public static DayOfWeek SUNDAY = new DayOfWeek("Sunday");
    public static DayOfWeek MONDAY = new DayOfWeek("Monday");
    public static DayOfWeek TUESDAY = new DayOfWeek("Tuesday");
    public static DayOfWeek WEDNESDAY = new DayOfWeek("Wednesday");
    public static DayOfWeek THURSDAY = new DayOfWeek("Thursday");
    public static DayOfWeek FRIDAY = new DayOfWeek("Friday");
    public static DayOfWeek SATURDAY = new DayOfWeek("Saturday");

    @Override
    public String toString() {
        return "enums.classenum.DayOfWeek{" +
                "title='" + title + '\'' +
                '}';
    }

}
