package enums.extended;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        // all objects
        System.out.println(Arrays.toString(DayOfWeek.values()));

        // index of particular
        int sundayIndex = DayOfWeek.SATURDAY.ordinal();
        System.out.println(sundayIndex);

        // whole object
        DayOfWeek returnedDay = DayOfWeek.valueOf("FRIDAY");
        System.out.println(returnedDay);
    }
}
