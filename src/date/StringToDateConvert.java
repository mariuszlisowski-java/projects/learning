package date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class StringToDateConvert {
    public static void main(String[] args) throws ParseException {
        String dateString = "1-MAY-2013";    // ok
        // String dateString = "MAY-1-2013"; // ok
        // String dateString = "MAY 1 2013"; // ok
        // String dateString = "MAY-2013-1"; // error

        Date date = new Date(dateString);

        System.out.println(date);
    }

}
