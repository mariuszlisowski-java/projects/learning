package date;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DayOfTheWeek {
    private final String pattern = "dd MMMM yyyy, EEEE";
    private Date currentDate;
    private int temperature;
    private String message;

    public static void main(String[] args) {
        DayOfTheWeek dayOfTheWeek = new DayOfTheWeek(28);
        System.out.println(dayOfTheWeek.message);
    }

    public DayOfTheWeek(int temperature) {
            this.currentDate = new Date();
            this.temperature = temperature;

            message = "Today is %s, current temeratuse is %s C";
            SimpleDateFormat format = new SimpleDateFormat(pattern);
            this.message = String.format(message, format.format(currentDate), temperature);
    }

}


