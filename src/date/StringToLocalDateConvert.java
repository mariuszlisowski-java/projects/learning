package date;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Locale;

public class StringToLocalDateConvert {
    public static void main(String[] args) throws ParseException {
        String dateStringA = "1 Apr 2020";        // ok
        // String dateString = "1 April 2020";    // error
        LocalDate localDateA = parseStringToLocalDateA(dateStringA);
        System.out.println(localDateA);

        String dateStringB = "16/08/2016";
        LocalDate localDateB = parseStringToLocalDateB(dateStringB);
        System.out.println(localDateB);
    }

    public static LocalDate parseStringToLocalDateA(String datePattern) throws ParseException {
        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .parseCaseInsensitive()
                .appendPattern("d MMM yyyy")
                .toFormatter(Locale.ENGLISH);

        return LocalDate.parse(datePattern, formatter);
    }

    public static LocalDate parseStringToLocalDateB(String datePattern) throws ParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");

        return LocalDate.parse(datePattern, formatter);
    }

}
