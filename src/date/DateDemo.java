package date;

import java.util.Date;

public class DateDemo {
    public static void main(String[] args) throws InterruptedException {
        Date date = new Date();
        System.out.println(date);

        // unix time
        System.out.println(date.getTime()); // miliseconds form January 1, 1970


        Date date1 = new Date();
        Thread.sleep(2000); //2 seconds
        Date date2 = new Date();
        System.out.println(date1.before(date2));
        System.out.println(date1.after(date2));
        System.out.println(date1.equals(date2));
    }

}
