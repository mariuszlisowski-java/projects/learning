package date;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

public class DaysThisYear {
    public static void main(String[] args) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        long daysPassed = daysPassedCurrentYear(currentYear);

        System.out.println(daysPassed + " days passed in " + currentYear);
    }

    public static long daysPassedCurrentYear(int currentYear) {
        LocalDate today = LocalDate.now();

        LocalDate firstDayOfCurrentYear = LocalDate.of(currentYear, Month.JANUARY, 1);

        return ChronoUnit.DAYS.between(firstDayOfCurrentYear, today);
    }

}
