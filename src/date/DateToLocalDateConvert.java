package date;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class DateToLocalDateConvert {
    public static void main(String[] args) {
        String dateString = "OCTOBER 1 2020";
        LocalDate localDate = convertDateToLocalDate(dateString);

        System.out.println(localDate);
    }

    public static LocalDate convertDateToLocalDate(String datePattern) {
        Date date = new Date(datePattern);

        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

}
