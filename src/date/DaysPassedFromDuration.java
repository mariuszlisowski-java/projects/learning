package date;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class DaysPassedFromDuration {
    public static void main(String[] args) {
        String dateString = "OCTOBER 1 2020";
        long daysPassed = daysPassedFromDate(dateString);

        System.out.println(daysPassed + " days passed from " + dateString);
    }

    public static long daysPassedFromDate(String datePattern) {
        Date date = new Date(datePattern);

        LocalDateTime passedDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime today = LocalDateTime.now();

        Duration duration = Duration.between(passedDate, today); // not for LocalDate

        return duration.toDays();
    }

}
