package date;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class DaysPassedFromChrono {
    public static void main(String[] args) {
        String dateString = "OCTOBER 1 2020";
        long daysPassed = daysPassedFromDate(dateString);

        System.out.println(daysPassed + " days passed from " + dateString);
    }

    public static long daysPassedFromDate(String datePattern) {
        Date date = new Date(datePattern);

        LocalDate passedDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate today = LocalDate.now();

        long duration = ChronoUnit.DAYS.between(passedDate, today); // also for LocalDateTime

        return duration;
    }

}
