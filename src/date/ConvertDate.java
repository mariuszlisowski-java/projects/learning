package date;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ConvertDate {
    public static void main(String[] args) {
        String inputDate = "2020-10-29";

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
//        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
//        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy", new Locale("pl"));
//        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy", Locale.forLanguageTag("pl"));

        Date date = null;

        try {
            date = inputFormat.parse(inputDate);
        } catch (ParseException exception) {
            System.out.println("Incorrect date format!");
        }

        String outputDate = outputFormat.format(date).toUpperCase();

        System.out.println(inputDate);
        System.out.println(outputDate);

    }
}
