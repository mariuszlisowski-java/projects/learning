package date;

import java.util.Date;

public class DateExample {
    public static void main(String[] args) throws InterruptedException {
        Date date = new Date();

        // current time & date
        System.out.println("Aktualna data: " + date);

        // difference
        Date currentTime = new Date();
        Thread.sleep(1000);
        Date newTime = new Date();

        long diff = newTime.getTime() - currentTime.getTime();
        System.out.println("Różnica: " + diff + " milisekund");

        if (newTime.after(currentTime)) {
            System.out.println("Nowy czas!");
        }

        // time passed today
        Date timeNow = new Date();
        int hours = timeNow.getHours(); // deprecated
        int minutes = timeNow.getMinutes(); // deprecated
        int seconds = timeNow.getSeconds(); // deprecated

        System.out.println("Time passed from midnight: ");
        System.out.println(hours + ":" + minutes + ":" + seconds);

        // time passed this year
        Date yearStartTime = new Date();
        yearStartTime.setHours(0); // deprecated
        yearStartTime.setMinutes(0);  // deprecated
        yearStartTime.setSeconds(0); // deprecated

        yearStartTime.setDate(1);      // first day of the month
        yearStartTime.setMonth(0);     // January (0 - 11)

        Date currTime = new Date();
        long difference = currentTime.getTime() - yearStartTime.getTime();

        long milisecondsInDay = 24 * 60 * 60 * 1000;

        int daysCounter = (int) (difference/milisecondsInDay); // full days
        System.out.println("Days passed this year: " + daysCounter);
    }

}
