package date;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatParserString {
    public static void main(String[] args) {
        String inputDate = "08-2020-1"; // ugly, needs to be formatted
        Date outputDate = null;
        try {
            outputDate = parseStringToDate(inputDate);
            System.out.println(outputDate);
        } catch (ParseException exception) {
            System.out.println("Unexpected date format!");
        }

        // reverse formatting
        System.out.println(parseDateToString(outputDate));
    }

    // string to date
    public static Date parseStringToDate(String inputDate) throws ParseException {
        DateFormat inputFormat = new SimpleDateFormat("MM-yyyy-dd");

        return inputFormat.parse(inputDate);
    }

    // date to string
    public static String parseDateToString(Date date) {
        DateFormat inputFormat = new SimpleDateFormat("MM-yyyy-dd");
        String outputDate = inputFormat.format(date);

        return outputDate;
    }

}
