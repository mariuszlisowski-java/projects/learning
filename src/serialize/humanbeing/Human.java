package serialize.humanbeing;

import java.io.*;

public class Human extends Mammal implements Serializable {
    private static long serialVersionUID = 100L; // not serialized
    private transient Integer age; // do not serialize (ageing process)
    private String name;

    public Human(Integer age, String name) {
        super(); // default constructor of base class call required
        this.age = age;
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public long getSerialVersionUID() {
        return serialVersionUID;
    }

    public void setSerialVersionUID(long serialVersionUID) {
        Human.serialVersionUID = serialVersionUID;
    }

    public static void main(String[] args) {
        Human human = new Human(44, "John");
        System.out.println("Static intial UID: " + human.getSerialVersionUID());

        // serialize class
        try (ObjectOutputStream output = new ObjectOutputStream(
                new FileOutputStream("human.serial")))
        {
            output.writeObject(human);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        Human readHuman = null;

        // deserialize class
        try (ObjectInputStream input = new ObjectInputStream(
                new FileInputStream("human.serial")))
        {
            readHuman = (Human) input.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        human.setSerialVersionUID(200L); // static attribute changed

        Integer age = readHuman.getAge(); // default null because not serialized
        long serial = readHuman.getSerialVersionUID(); // currect class definition value
        String name = readHuman.getName(); // ok, serialized

        System.out.println("Attribure 'name read from file: " + name);
        System.out.println("Attrubute 'age' read from file: " + age);
        System.out.println("Static read from file UID: " + serial);
    }

}
