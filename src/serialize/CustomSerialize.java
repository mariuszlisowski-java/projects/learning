package serialize;

import java.io.*;

public class CustomSerialize implements Serializable {
    // not serializable
    public static class A {
        protected String nameA = "A";

        public A() {}   // compulsory default constructor
        public A(String nameA) {
            this.nameA += nameA;
        }
    }

    public class B extends A implements Serializable {
        private String nameB;
        public B(String nameA, String nameB) {
            super(nameA);
            this.nameA = nameA;
            this.nameB = nameB;
        }

        // custom write (must be private)
        private void writeObject(ObjectOutputStream oos) throws IOException {
            oos.writeObject(nameA);     // serialize variable from classA
            oos.writeObject(nameB);
//            oos.defaultWriteObject();
            oos.close();
        }

        // custom read (must be private)
        private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
            nameA = (String) ois.readObject();
            nameB = (String) ois.readObject();
//            ois.defaultReadObject();
            ois.close();
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);

        CustomSerialize custom = new CustomSerialize();
        B b0 = custom.new B("B2", "C33");
        System.out.println("nameA: " + b0.nameA + ", nameB: " + b0.nameB);

        oos.writeObject(b0);

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bais);

        B b1 = (B)ois.readObject();
        System.out.println("nameA: " + b1.nameA + ", nameB: " + b1.nameB);
    }

}
