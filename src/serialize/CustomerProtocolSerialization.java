package serialize;

import java.io.*;

public class CustomerProtocolSerialization implements Externalizable {
    private String field;

    // compulsory unparametrized constructor
    public CustomerProtocolSerialization() {
    }
    // another constructor
    public CustomerProtocolSerialization(String field) {
        this.field = field;
    }
    // methods
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(field);
    }
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        field = in.readUTF();
    }

    // main
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        CustomerProtocolSerialization object = new CustomerProtocolSerialization("field value");

        try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("externalizable.bin"))) {
            output.writeObject(object);
        }

        try (ObjectInputStream input = new ObjectInputStream(new FileInputStream("externalizable.bin"))) {
            CustomerProtocolSerialization readObject = (CustomerProtocolSerialization) input.readObject();
            System.out.println(readObject.field);
        }
    }

}
