package serialize;

import java.io.*;
import java.util.Base64;

public class Base64Example {

    public static void main(String[] args) throws IOException {
        City city = new City("Sosnowiec");

        // write object
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(arrayOutputStream);
        objectOutputStream.writeObject(city);

        // print binary
        System.out.println("Binary:");
        System.out.println(arrayOutputStream.toString());

        // print converted text
        System.out.println("Base64:");
        String encoded = Base64.getEncoder().encodeToString(arrayOutputStream.toByteArray());
        System.out.println(encoded);

    }

    public static class City implements Serializable {
        private String name;

        public City(String name) {
            this.name = name;
        }
    }
}
