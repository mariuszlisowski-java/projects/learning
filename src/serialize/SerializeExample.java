package serialize;

import java.io.*;

public class SerializeExample {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        String filename = "test.class";

        // write object
        OutputStream outputStream = new FileOutputStream(filename);
        ObjectOutputStream objectOutStream = new ObjectOutputStream(outputStream);
        objectOutStream.writeObject(new Cat("Filemon"));
        objectOutStream.close();
        outputStream.close();

        // read object
        InputStream inputStream = new FileInputStream(filename);
        ObjectInputStream objectInStream = new ObjectInputStream(inputStream);
        Object readObject = objectInStream.readObject();
        objectInStream.close();
        inputStream.close();

        // create new object
        Cat cat = (Cat) readObject;
        System.out.println(cat.name);
    }

    public static class Cat implements Serializable {
        private String name;

        public Cat(String name) {
            this.name = name;
        }
    }
}
