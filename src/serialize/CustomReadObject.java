package serialize;

import java.io.*;

public class CustomReadObject implements Serializable, AutoCloseable {
    transient private FileOutputStream stream;
    private String filename;

    public CustomReadObject(String filename) throws FileNotFoundException {
        this.stream = new FileOutputStream(filename);
        this.filename = filename;
    }

    public void writeToFile(String string) throws IOException {
        stream.write(string.getBytes());
        stream.write("\n".getBytes());
        stream.flush(); // writes but keeps stream open
    }

    // does nothing more (can be omitted)
    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
    }

    // reads object & initializes the stream
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.stream = new FileOutputStream(filename, true);
    }

    @Override
    public void close() throws Exception {
        System.out.println("Closing everything!");
        stream.close();
    }

    public static void main(String[] args) throws Exception {
        String filename = "znaczki";

        CustomReadObject croA = new CustomReadObject(filename);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);

        croA.writeToFile("1st string written to file");
        croA.writeToFile("2nd string written to file");
        // using private writeObject(..) method first
        oos.writeObject(croA); // serialize in byte array stream
        croA.close();

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bais);
        // using private readObject(...) method first
        CustomReadObject croB = (CustomReadObject) ois.readObject();   // deserialize
        croB.writeToFile("3rd string written to file");
        croB.writeToFile("4th string written to file");
        croB.close();
    }

}
