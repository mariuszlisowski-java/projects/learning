package serialize;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TransientExample implements Serializable {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        TransientExample savedObject = new TransientExample(28);
        System.out.println(savedObject);

        String filename = "test.class";

        OutputStream outputStream = new FileOutputStream(filename);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(savedObject);
        objectOutputStream.close();
        outputStream.close();

        InputStream inputStream = new FileInputStream(filename);
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        Object loadedObject = objectInputStream.readObject();
        objectInputStream.close();
        inputStream.close();

        // check if deserialized the same object
        System.out.println(savedObject.toString().equals(loadedObject.toString()) ? "same" : "not");
        System.out.println(loadedObject);

//        TransientExample newTransientExample = new TransientExample(33);
//        System.out.println(newTransientExample);
    }

    private transient final String pattern = "dd MMMM yyyy, EEEE";  // not serialized
    private transient Date currentDate;     // no need to serialize (used in a string)
    private transient int temperature;      // no need to serialize (used in a string)
    String string;                          // serialized

    public TransientExample(int temperature) {
        this.currentDate = new Date();
        this.temperature = temperature;

        string = "Today's %s, current temperature is %s C";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        this.string = String.format(string, format.format(currentDate), temperature);   // constructed here
    }

    @Override
    public String toString() {
        return this.string;
    }
}
