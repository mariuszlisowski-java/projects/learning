package serialize;

import java.io.*;

public class TransientStaticExample {

    public static void main(String[] args) throws Exception {
        Book book = new Book();
        book.setBookName("Java Reference");
        book.setDescription("will not be saved");
        book.setCopies(25);

        serialize(book, "test.class");
        Book ebook = deserialize("test.class");

        System.out.println(ebook.getBookName());            // serialized
        System.out.println(ebook.getDescription());         // null (transient)
        System.out.println(ebook.getCopies());              // zero (transient)
        System.out.println(ebook.getBookCategory());        // serialized (transient ignored)

        System.out.println(Book.getSerialVersionUID());     // not serialized (read from class)
        System.out.println(Book.getStaticString());         // not serialized (read from class)
    }

    public static void serialize(Book book, String filename) throws Exception {
        FileOutputStream file = new FileOutputStream(filename);
        ObjectOutputStream out = new ObjectOutputStream(file);
        out.writeObject(book);
        out.close();
        file.close();
    }

    public static Book deserialize(String filename) throws Exception {
        FileInputStream file = new FileInputStream(filename);
        ObjectInputStream in = new ObjectInputStream(file);
        Book book = (Book) in.readObject();
        in.close();
        file.close();

        return book;
    }

    // transient fields (not to be serialized)
    public static class Book implements Serializable {
        private static final long serialVersionUID = 293726549L;    // unique identifier for Serializable class
                                                                    // ... default generated if not declared
        public static String staticString = "Static";               // not serialized (because static)
        private String bookName;                                    // serialized
        private transient String description;                       // not to be serialized
        private transient int copies;                               // not to be serialized
        private final transient String bookCategory = "Fiction";    // transient ignored (because final)

        public static long getSerialVersionUID() { return serialVersionUID; }
        public static String getStaticString() { return staticString; }
        public String getBookName() { return bookName; }
        public String getDescription() { return description; }
        public int getCopies() { return copies; }
        public String getBookCategory() { return bookCategory; }


        public void setBookName(String bookName) { this.bookName = bookName; }
        public void setDescription(String description) { this.description = description; }
        public void setCopies(int copies) { this.copies = copies; }

    }
}
