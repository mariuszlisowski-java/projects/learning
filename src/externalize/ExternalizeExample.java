package externalize;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ExternalizeExample {
    public static class Apartment implements Externalizable {

        private Apartment apartment;
        private String street;
        private int number;
        private List<Apartment> apartments;

        // compulsory here
        public Apartment() { super(); }

        public Apartment(String street, int number) {
            this.street = street;
            this.number = number;
            apartments = new ArrayList<>();
        }

        public void setApartment(Apartment apartment) {
            this.apartment = apartment;
        }

        public void addApartment(Apartment apartment) {
            apartments.add(apartment);
        }

        public String toString() {
            return "Address: " + street + " " + number;
        }

        @Override
        public void writeExternal(ObjectOutput out) throws IOException {
            out.writeObject(apartment);
            // out.writeChars(street);  // faulty
            out.writeObject(street);
            out.writeInt(number);
            out.writeObject(apartments);

            out.flush();                        // all must be written
        }

        @Override
        public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
            apartment = (Apartment) in.readObject();
            // street = in.readLine();  // faulty
            street = (String) in.readObject();
            number = in.readInt();
            apartments = (List<Apartment>) in.readObject();
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Apartment apartment = new Apartment("High Street", 10);
        apartment.setApartment(new Apartment("Dead End", 777));
        apartment.addApartment(apartment);

        // save class state
        String filename = "test.class";
        apartment.writeExternal(new ObjectOutputStream(new FileOutputStream(filename)));
        System.out.println(apartment);

        // change class state
        apartment.street = "Empty";
        apartment.number = 0;
        System.out.println(apartment);

        // restore class state
        apartment.readExternal(new ObjectInputStream(new FileInputStream(filename)));
        System.out.println(apartment);
    }
}
