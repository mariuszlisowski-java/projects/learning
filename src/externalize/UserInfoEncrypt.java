package externalize;

import java.io.*;
import java.util.Base64;

public class UserInfoEncrypt implements Externalizable {
    private final int FINAL_IMPOSSIBLE_TO_DESERIALIZE = 0;   // but serialized when Serializable
    private String firstName;
    private String lastName;
    private String passport;

    private static final long serialVersionUID = 1L;    // can be any long number

    // compulsory default (called first)
    public UserInfoEncrypt() {}

    public UserInfoEncrypt(String name, String surname, String passportNo) {
        firstName = name;
        lastName = surname;
        passport = passportNo;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(this.getFirstName());
        out.writeObject(this.getLastName());
        out.writeObject(this.encryptString(this.getPassport()));
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        firstName = (String) in.readObject();
        lastName = (String) in.readObject();
        passport = this.decryptString((String) in.readObject());
    }

    private String encryptString(String data) {
        String encryptedData = Base64.getEncoder().encodeToString(data.getBytes());
        System.out.println("Encrypted passport no.: " + encryptedData);
        return encryptedData;
    }

    private String decryptString(String data) {
        String decrypted = new String(Base64.getDecoder().decode(data));
        System.out.println("Decrypted passport no.: " + decrypted);
        return decrypted;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassport() {
        return passport;
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        UserInfoEncrypt userInfo = new UserInfoEncrypt("Paul", "Atreides", "PL987654321");
        String filename = "test.class";

        // externalization
        FileOutputStream fileOutputStream = new FileOutputStream(filename);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(userInfo);
        objectOutputStream.close();

        // deexternalization
        FileInputStream fileInputStream = new FileInputStream(filename);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        UserInfoEncrypt userInfoLoaded = (UserInfoEncrypt) objectInputStream.readObject();
        objectInputStream.close();

    }
}
