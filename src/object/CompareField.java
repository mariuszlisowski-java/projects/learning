package object;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;

public class CompareField {
    public static void main(String[] args) {
        List<Car> cars = new ArrayList<>() {{
            add(new Car("Toyota", 29_000));
            add(new Car("Isuzu", 35_000));
        }};

        System.out.println(getMostExpensiveCar(cars));
    }

    private static Car getMostExpensiveCar(List<Car> cars) {
        Car mostExpensiveCar = Collections.max(cars, Comparator.comparing(s -> s.getPrice()));

        return mostExpensiveCar;
    }

    public static class Car {
        private int price;
        private String make;

        public Car(String make, int price) {
            this.make = make;
            this.price = price;
        }

        public int getPrice() {
            return price;
        }

        public String toString() {
            return make + ", price $" + price;
        }
    }
}
