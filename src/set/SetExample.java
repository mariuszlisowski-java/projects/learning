package set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetExample {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("one");
        set.add("two");
        set.add("three");
        // iterator
        Iterator<String> it = set.iterator();
        while (it.hasNext()) {
            String text = it.next();
            System.out.println(text);
        }
        // whole set
        System.out.println(set);
        // foreach
        for (String el : set) {
            System.out.println(el);
        }

    }
}
