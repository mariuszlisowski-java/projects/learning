package set;

import java.util.HashSet;
import java.util.Random;

public class HashSetExample {
    public static HashSet<Integer> createSet() {
        HashSet<Integer> set = new HashSet<>();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            set.add(random.nextInt(99) + 1); // 1-100
        }

        return set;
    }

    public static HashSet<Integer> removeAllLargerThan10(HashSet<Integer> set) {
        set.removeIf(el -> el > 10);

        return set;
    }

    public static void main(String[] args) {
        HashSet<Integer> set = createSet();
        for (Integer el : set) {
            System.out.print(el + " ");
        }
        System.out.println("\nSet size: " + set.size());
        System.out.print("Left: ");
        for (Integer el : removeAllLargerThan10(set)) {
            System.out.print(el + " ");
        }

    }
}
