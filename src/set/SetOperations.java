package set;

import java.util.HashSet;
import java.util.Set;

public class SetOperations {
    public static void main(String[] args) {
        Set<Cat> cats = createCats();
        Set<Dog> dogs = createDogs();

        Set<Object> animals = joinCatsAndDogs(cats, dogs);
        printAnimals(animals);

        removeCats(animals, cats);
        printAnimals(animals);
    }

    public static Set<Cat> createCats() {
        HashSet<Cat> output = new HashSet<Cat>();

        output.add(new Cat("Oli"));
        output.add(new Cat("Ali"));
        output.add(new Cat("Eli"));

        return output;
    }

    public static Set<Dog> createDogs() {
        HashSet<Dog> output = new HashSet<>();

        output.add(new Dog("Lar"));
        output.add(new Dog("Sar"));
        output.add(new Dog("Mar"));

        return output;
    }

    public static Set<Object> joinCatsAndDogs(Set<Cat> cats, Set<Dog> dogs) {
        Set<Object> output = new HashSet<>();
        output.addAll(cats);
        output.addAll(dogs);

        return output;
    }

    public static void removeCats(Set<Object> cats, Set<Cat> dogs) {
        cats.removeAll(dogs);
    }

    public static void printAnimals(Set<Object> animals) {
        for (Object ob : animals) {
            System.out.println(ob);
        }
        System.out.println();
    }

    public static class Cat {
        private String name;

        Cat(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Cat " + name;
        }
    }
    public static class Dog {
        private String name;

        Dog(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Dog " + name;
        }
    }

}
