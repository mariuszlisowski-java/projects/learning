package unmutable;

// enable VM option -ea to use assertions
public class StringUnmutable {
    public static void main(String[] args) {
        String name = "Alice";

        name.replace("ce", "sson");   // not changed
        assert name.equals("Alisson") : "Sorry, unmutable!";

        // proper usage
        String replaced = name.replace("ce", "sson");
        assert replaced.equals("Alisson") : "Not equal";
    }
}
