package unmutable;

// enable VM option -ea to use assertions
public class ReferenceUnmutable {
    public static void main(String[] args) {
        final String name = "Emily";  // unmutable reference
        // name = "Joan"; // ERROR

        final int[] array = {1, 2, 3}; // unmutable reference
        // array = new int[] {4, 5, 6}; ERROR
        array[0] = 4; // internal state of the object is mutable!
        array[1] = 5;
        array[2] = 6;

        assert array[0] == 4; // ok
        assert array[1] == 2 : "Sorry, value changed!"; // is 5 now
    }
}
