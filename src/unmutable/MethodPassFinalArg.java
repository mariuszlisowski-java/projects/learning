package unmutable;

public class MethodPassFinalArg {
    public static void main(String[] args) {
        final int[] array = {1, 2, 3};

        // final array passed (by value0
        doNotChangeArgs(array);
    }

    // reference copied
    public static void doNotChangeArgs(int[] arr) {
        // original array
        arr[0] = 4;  // internal state of the object is mutable!

        // new array
        arr = new int[] {4, 5, 6}; // copy can be changed
    }

}
