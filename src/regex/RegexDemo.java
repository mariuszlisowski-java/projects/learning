package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexDemo {
    public static void main(String[] args) {
        String str = "ala ma kota ala";

        String regex1 = "(ala)";    // all occurences
        String regex2 = "(^ala)";   // first occurence
        String regex3 = "(ala$)";   // last occurence
        String regex4 = "(^.l)";    // ??
        String regex5 = "(l.$)";    // l + any letter end

        System.out.println(regexReplaceAll(str, regex1));
        System.out.println(regexReplaceAll(str, regex2));
        System.out.println(regexReplaceAll(str, regex3));
        System.out.println(regexReplaceAll(str, regex4));
        System.out.println(regexReplaceAll(str, regex5));
    }

    public static boolean regexFound(String str, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);

        return matcher.find();
    }

    public static String regexReplaceAll(String str, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);

        return str.replaceAll(regex, "");
    }

}
