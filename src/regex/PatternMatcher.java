package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternMatcher {
    public static void main(String[] args) {
        Pattern pattern1 = Pattern.compile("(a{3})"); // three 'a'
        Matcher matcher1 = pattern1.matcher("aaabbbccc");
        System.out.println(matcher1.find()); // finds substring
        System.out.println(matcher1.groupCount());

        Pattern pattern2 = Pattern.compile("(b{4})"); // four 'b'
        Matcher matcher2 = pattern2.matcher("bbbb");
        System.out.println(matcher2.matches()); // matches whole string

        System.out.println("-----------");

        Pattern pattern3 = Pattern.compile("Jan", Pattern.CASE_INSENSITIVE);
        Matcher matcher3 = pattern3.matcher("nazywam się jan kowalski");
        System.out.println(matcher3.find()); // 'Jan' found

        System.out.println("-----------");

        Pattern pattern4 = Pattern.compile("(\\d{4})-(\\d{2})-(\\d{2})");
        Matcher matcher4 = pattern4.matcher("Dzisiaj jest 2019-02-31");
        if (matcher4.find() && matcher4.groupCount() <= 3) {
            System.out.println(matcher4.group(1));
            System.out.println(matcher4.group(2));
            System.out.println(matcher4.group(3));
        }
    }

}
