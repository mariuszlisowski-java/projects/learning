package regex;

public class RegexExample {
    public static void main(String[] args) {
//        String str1 = "aaabbbccc";
        RegexExample str1 = new RegexExample("aaabbbccc");
        RegexExample str2 = new RegexExample("aaa123ccc");
        RegexExample str3 = new RegexExample("a b c d");
        RegexExample str4 = new RegexExample("$ 123 % abc ^ ABC &");
        RegexExample str5 = new RegexExample("123 abc ABC");
        RegexExample str6 = new RegexExample("30/06/2020");

        String regex1 =  "(a{3})";   //  1. three 'a'
        String regex2 =  "(b{3,})";  //  2. at lest one 'b'
        String regex3 =  "(c{1,3})"; //  3. min one, max three 'b'

        String regex4 =  "(\\d)";    //  4. any digit
        String regex5 =  "(\\D)";    //  5. NOT above
        String regex6 =  "(\\s)";    //  6. \t \n \b space
        String regex7 =  "(\\S)";    //  7. NOT above
        String regex8 =  "(\\w)";    //  8. [a-zA-Z_0-9]
        String regex9 =  "(\\W)";    //  9. NOT above
        String regex10 = "(\\b)";    // 10. borders [a-zA-Z_0-9]
        String regex11 = "(\\d{4})"; // 11. four digits
        String regex12 = "(\\d{2})/(\\d{2})/(\\d{4})"; // 12. date

        str1.replaceAllAndPrint(regex1, "!");
        str1.replaceAllAndPrint(regex2, "!");
        str1.replaceAllAndPrint(regex3, "!");

        str2.replaceAllAndPrint(regex4, "!");
        str2.replaceAllAndPrint(regex5, "!");
        str3.replaceAllAndPrint(regex6, "!");
        str3.replaceAllAndPrint(regex7, "!");
        str4.replaceAllAndPrint(regex8, "!");
        str4.replaceAllAndPrint(regex9, "!");
        str5.replaceAllAndPrint(regex10, "!");
        str6.replaceAllAndPrint(regex11, "!!!!");
        str6.replaceAllAndPrint(regex12, "$3-$2-$1"); // reverse order
    }

    private String str;
    private static int counter = 0;

    public RegexExample(String str) {
        this.str = str;
    }

    public void replaceAllAndPrint(String regex, String replacement) {
        String output = str.replaceAll(regex, replacement);
        System.out.println((++counter) + ". " + output);
    }

}
