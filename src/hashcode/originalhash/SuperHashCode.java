package hashcode.originalhash;

public class SuperHashCode {
    private String word;

    public SuperHashCode(String word) {
        this.word = word;
    }

    // custom method with original hash returned
    @Override
    public int hashCode() {
        int hash = super.hashCode();
        System.out.println("original hashCode() called - computed hash: " + hash);

        return hash;
    }

}
