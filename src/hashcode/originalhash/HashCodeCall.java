package hashcode.originalhash;

import java.util.HashSet;
import java.util.Set;

public class HashCodeCall {
    public static void main(String[] args) {
        Set<SuperHashCode> words  = new HashSet<>();

        SuperHashCode code1 = new SuperHashCode("Welcome");
        SuperHashCode code2 = new SuperHashCode("to");
        SuperHashCode code3 = new SuperHashCode("Matrix");

        words.add(code1);
        words.add(code2);
        words.add(code3);

        if (words.contains(code3)) {
            System.out.println("Matrix is in the list");
        }
    }


}
