package hashcode.hashcodecustom;

import java.util.HashMap;
import java.util.Map;

// custom Object.hashCode method
public class HashingTestSuccess {
    public static void main(String[] args) {
        Map<DataKeyCustom, Integer> map = getAllData();

        DataKeyCustom dataKey = new DataKeyCustom();
        dataKey.setId(1);
        dataKey.setName("Data");
        System.out.println(dataKey.hashCode()); // custom hashCode() used

        // same hash codes thanks custom method in DataKeyCustom
        Integer value;
        if ((value = map.get(dataKey)) == null) {
            System.out.println("Key not found");
        } else {
            System.out.println("Key found: " + value);
        }
    }

    private static Map<DataKeyCustom, Integer> getAllData() {
        Map<DataKeyCustom, Integer> map = new HashMap<>();

        DataKeyCustom dataKey = new DataKeyCustom();
        dataKey.setId(1);
        dataKey.setName("Data");
        System.out.println(dataKey.hashCode()); // custom hashCode() used

        map.put(dataKey, 10);

        return map;
    }
    
    
}
