package hashcode.examples;

public class UserIdea {
    private long id;
    private String name;
    private String email;

    public UserIdea(long id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    // IntelliJ hash code implementation
    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + name.hashCode();
        result = 31 * result + email.hashCode();

        return result;
    }
}
