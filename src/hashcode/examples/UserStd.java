package hashcode.examples;

public class UserStd {
    private long id;
    private String name;
    private String email;

    public UserStd(long id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    // standard hash code implementation
    @Override
    public int hashCode() {
        int hash = 7;
        // custom hash computation
        hash = 31 * hash + (int) id;
        hash = 31 * hash + (name == null ? 0 : name.hashCode()); // String hashCode() called
        hash = 31 * hash + (email == null ? 0 : email.hashCode());  // String hashCode() called

        System.out.println("hashCode() called - computed hash: " + hash);

        return hash;
    }

}
