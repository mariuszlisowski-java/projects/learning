package hashcode.examples;

import java.util.HashMap;
import java.util.Map;

public class HashCodeCall {
    public static void main(String[] args) {
        Map<UserStd, UserStd> users = new HashMap<>();

        UserStd userStd1 = new UserStd(1L, "John", "john@domain.com");
        UserStd userStd2 = new UserStd(2L, "Jennifer", "jennifer@domain.com");
        UserStd userStd3 = new UserStd(3L, "Mary", "mary@domain.com");

        users.put(userStd1, userStd1);
        users.put(userStd2, userStd2);
        users.put(userStd3, userStd3);

        // calls hashCode() method for every entry plus for found entry
        if (users.containsKey(userStd2)) {
            System.out.print("User found in the collection");
        }
    }

}
