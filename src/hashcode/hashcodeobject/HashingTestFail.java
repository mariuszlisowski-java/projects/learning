package hashcode.hashcodeobject;

import java.util.HashMap;
import java.util.Map;

// default Object.hashCode method
public class HashingTestFail {
    public static void main(String[] args) {
        Map<DataKeyObject, Integer> map = getAllData();

        // same key created as in the method below
        DataKeyObject dataKey = new DataKeyObject();
        dataKey.setId(1);
        dataKey.setName("Data");
        System.out.println(dataKey.hashCode()); // Object.hashCode() used

        // but they have different hash codes
        Integer value;
        if ((value = map.get(dataKey)) == null) {
            System.out.println("Key not found");
        } else {
            System.out.println("Key found: " + value);
        }

    }

    private static Map<DataKeyObject, Integer> getAllData() {
        Map<DataKeyObject, Integer> map = new HashMap<>();

        DataKeyObject dataKey = new DataKeyObject();
        dataKey.setId(1);
        dataKey.setName("Data");
        System.out.println(dataKey.hashCode()); // Object.hashCode() used

        map.put(dataKey, 10);

        return map;
    }
}
