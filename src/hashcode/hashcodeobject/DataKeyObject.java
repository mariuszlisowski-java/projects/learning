package hashcode.hashcodeobject;

public class DataKeyObject {
    private String name;
    private int id;

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "DataKey{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }


}
