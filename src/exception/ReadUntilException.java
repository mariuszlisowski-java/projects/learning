package exception;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ReadUntilException {
    public static void main(String[] args) {
        readFromUser();
    }

    public static void readFromUser() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<Integer> list = new ArrayList<>();
        try {
            while (true) {
                System.out.print(": ");
                list.add(Integer.parseInt(reader.readLine()));
            }
        } catch (NumberFormatException | IOException exception) {
            System.out.println("Interrupted by non-number entry");
        } finally {
            for (Integer number : list) {
                System.out.println(number);
            }
        }

    }
}
