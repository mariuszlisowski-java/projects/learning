package exception;

public class HoursToMinutes {
    public static void main(String[] args) {
        int hours = -2; // not accepted in a method
        int numberOfSeconds = 0;

        HoursToMinutes hoursToMinutes = new HoursToMinutes();

        try {
            numberOfSeconds = hoursToMinutes.getNumberOfSeconds(hours);
        }
        catch (IllegalArgumentException exception) {
            System.out.println("Exception caught - fixed!");
            hours = -hours;
            numberOfSeconds = hoursToMinutes.getNumberOfSeconds(hours);
        }
        System.out.println(hours + "h is " + numberOfSeconds + " seconds");
    }

    // accepts only positive values and zero
    public int getNumberOfSeconds(int hour) {
        if (hour < 0) {
            throw new IllegalArgumentException("Hour must be >= 0: " + hour);
        }
        return hour * 60 * 60;
    }
}
