package exception;

public class UncheckedExceptions {
    public static void main(String[] args) {}

    // Exception/RuntimeException
    static class MyException1 extends RuntimeException {}

    static class MyException2 extends IllegalArgumentException {}
    static class MyException3 extends NumberFormatException { }

    static class MyException4 extends IndexOutOfBoundsException {}
    static class MyException5 extends ArrayIndexOutOfBoundsException {}

    static class MyException6 extends ArithmeticException {}
}
