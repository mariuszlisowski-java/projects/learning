package exception;

public class StackTrace {
    public static void main(String[] args) {
        handleExceptions(new StackTrace());
    }

    public static void handleExceptions(StackTrace obj) {
        try {
            obj.method1(); // exception thrown
            obj.method2();
            obj.method3();
        } catch (NullPointerException | IndexOutOfBoundsException | NumberFormatException exception) {
            printStack(exception);
        }
    }

    public static void printStack(Throwable throwable) {
        System.out.println(throwable);
        for (StackTraceElement element : throwable.getStackTrace()) {
            System.out.println(element);
        }
    }

    public void method1() {
        throw new NullPointerException();
    }

    public void method2() {
        throw new IndexOutOfBoundsException();
    }

    public void method3() {
        throw new NumberFormatException();
    }
}
