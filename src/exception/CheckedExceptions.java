package exception;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketException;
import java.nio.file.FileSystemAlreadyExistsException;

public class CheckedExceptions {
    public static void main(String[] args) {}

    // Exception/IOException
    static class MyException1 extends IOException {}
    static class MyException2 extends NoSuchFieldException {}
    static class MyException3 extends FileNotFoundException {}
    static class MyException4 extends FileSystemAlreadyExistsException {}
    static class MyException5 extends SocketException { }
}
