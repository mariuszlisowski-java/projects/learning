package exception;

import java.io.IOException;

public class ThrowAgain {
    public static StatelessBean BEAN = new StatelessBean();

    public static void main(String[] args) {
        try {
            handleException();
        } catch (IOException exception) {
            BEAN.log(exception); // caught again
        }
    }

    public static void handleException() throws IOException {
        try {
            BEAN.throwException();
        } catch (IOException exception) {
            BEAN.log(exception);
            throw exception; // throws the same exception again
        }
    }

    public static class StatelessBean {
        public void log(Exception exception) {
            System.out.println(exception.getMessage() + ", " + exception.getClass().getSimpleName());
        }

        public void throwException() throws IOException {
            throw new IOException("Original exception");
        }
    }
}
