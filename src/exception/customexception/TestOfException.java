package exception.customexception;

public class TestOfException {
    public static void main(String[] args) {
        try {
            throwException();
        } catch (CustomException exception) {
            System.out.println(exception);
        }
    }

    public static void throwException() throws CustomException {
        throw new CustomException("Really bad thing happened!");
    }
}
