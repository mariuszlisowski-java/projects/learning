package exception;

import java.util.InputMismatchException;
import java.util.Scanner;

public class DoubleException {
    public static void main(String[] args) {
//        System.out.println(doubleException());
        System.out.println(withoutException());
    }

    public static int doubleException() {
        Scanner scanner = new Scanner(System.in);
        int value = 0;
        try {
            System.out.print("Enter integer...\n: ");
            value = scanner.nextInt();
        } catch (InputMismatchException exception) {
            scanner.nextLine(); // clear buffer
            System.out.print("Integer expected...\n: ");
            value = scanner.nextInt();
        }

        return value;
    }

    public static int withoutException() {
        Scanner scanner = new Scanner(System.in);
        int value = 0;
        while (true) {
            System.out.print("Enter integer...\n: ");
            if (scanner.hasNextInt()) {
                value = scanner.nextInt();
                break;
            } else {
                System.out.println("Integer expected...");
            }
            scanner.next(); // clear incorrect entry
        }

        return value;
    }
}
