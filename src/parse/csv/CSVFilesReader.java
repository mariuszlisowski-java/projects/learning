package parse.csv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVFilesReader {
    private static final String COMMA_DELIMITER = ",";

    public static void main(String[] args) throws IOException {
        List<List<String>> lists = parseCSV("test.csv");

        for (List<String> row : lists) {
            for (String entry : row) {
                System.out.print(entry + ",");
            }
            System.out.println();
        }
    }

    private static List<List<String>> parseCSV(String filename) throws IOException {
        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);
                records.add(Arrays.asList(values));
            }
        }

        return records;
    }

}
