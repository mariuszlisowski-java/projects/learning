package parse.properties;

import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class PropertiesSystemRead {

    public static void main(String[] args) throws Exception {
        // get all the system properties
        Properties properties = System.getProperties();

        // stores set of properties information
        Set set = properties.entrySet();

        // iterate over the set
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry)it.next();
            System.out.println(entry.getKey() + " = "
                    + entry.getValue());
        }
    }

}
