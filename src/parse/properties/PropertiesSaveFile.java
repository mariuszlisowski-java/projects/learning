package parse.properties;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class PropertiesSaveFile {

    public static void main(String[] args){
        // create an instance of Properties
        Properties properties = new Properties();

        // add properties to it
        properties.setProperty("name", "Raisin");
        properties.setProperty("email","raisin@gmail.com");

        // store the properties to a file
        try {
            properties.store(new FileWriter("info.properties"),
                    "properties example");
        } catch (IOException exception) {
            System.out.println("Write error!");
        }

        System.out.println("File written successfully!");
    }

}
