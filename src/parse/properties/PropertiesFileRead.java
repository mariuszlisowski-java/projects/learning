package parse.properties;

import java.io.*;
import java.util.Map;
import java.util.Properties;

public class PropertiesFileRead {

    public static void main(String[] args) throws Exception {
        String filename = "test.properties";
        FileInputStream inputStream = new FileInputStream(filename);

        printPropertiesFile(inputStream);
    }

    public static void printPropertiesFile(InputStream inputStream) throws Exception {
        Properties properties = new Properties();
        properties.load(inputStream);

        for (Map.Entry<Object, Object> property : properties.entrySet()) {
            System.out.println(property.getKey() + " = " + property.getValue());
        }
    }

}
