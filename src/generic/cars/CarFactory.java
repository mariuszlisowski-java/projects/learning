package generic.cars;

public class CarFactory<T, K> {
    private T body;
    private K engine;

    public CarFactory(T body, K engine) {
        this.body = body;
        this.engine = engine;
    }

    public T getBody() {
        return body;
    }

    public K getEngine() {
        return engine;
    }

}
