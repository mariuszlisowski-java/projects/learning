package generic.cars;

public class Main {
    public static void main(String[] args) {
        Body body = new Body("SUV");
        Engine engine = new Engine(210);

        CarFactory<Body, Engine> carManufactured = new CarFactory<>(body, engine);

        System.out.println(carManufactured.getBody().getType());
        System.out.println(carManufactured.getEngine().getPower());

    }
}
