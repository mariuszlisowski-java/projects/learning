package generic.cars;

public class Engine {
    private Integer power;

    public Engine(Integer power) {
        this.power = power;
    }

    public Integer getPower() {
        return power;
    }
}
