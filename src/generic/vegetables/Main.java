package generic.vegetables;

public class Main {
    public static void main(String[] args) {
        Carrot carrot = new Carrot("Rumba");
        Orange orange = new Orange("Risso");

        VegetableBox<Vegetable> vegetableBox = new VegetableBox<>(carrot); // but not orange
        FruitBox<Fruit> fruitBox = new FruitBox<>(orange); // but not carrot

        FancyFruitBox<Fruit> fancyFruitBox = new FancyFruitBox<>(orange);
        fancyFruitBox.showBoxColor();
    }
}
