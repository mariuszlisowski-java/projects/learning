package generic.vegetables;

public class Carrot implements Vegetable {
    private String name;

    public Carrot(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
