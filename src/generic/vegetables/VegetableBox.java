package generic.vegetables;

public class VegetableBox<T extends Vegetable> {
    private T item;

    public VegetableBox(T item) {
        this.item = item;
    }

}
