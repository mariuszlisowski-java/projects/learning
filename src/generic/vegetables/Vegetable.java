package generic.vegetables;

public interface Vegetable {
    public String getName();
}
