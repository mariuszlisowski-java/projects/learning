package generic.vegetables;

public class FruitBox<T extends Fruit> {
    private T item;

    public FruitBox(T item) {
        this.item = item;
    }
}
