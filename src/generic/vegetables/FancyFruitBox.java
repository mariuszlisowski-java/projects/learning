package generic.vegetables;

public class FancyFruitBox<T extends Fruit> extends FruitBox<T> {
    public FancyFruitBox(T item) {
        super(item);
    }

    public void showBoxColor() {
        System.out.println("Pink fancy fruit box");
    }
}
