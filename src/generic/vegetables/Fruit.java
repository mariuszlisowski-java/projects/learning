package generic.vegetables;

public interface Fruit {
    public String getName();
}
