package generic.vegetables;

public class Orange implements Fruit {
    private String name;

    public Orange(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
