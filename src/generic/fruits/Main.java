package generic.fruits;

public class Main {
    public static void main(String[] args) {
        FruitBox<Apple> appleBox = new FruitBox<>(new Apple());
        FruitBox<Banana> bananaBox = new FruitBox<>(new Banana());

        Apple apple = appleBox.getFruit();
        System.out.println(apple);

        Banana banana = bananaBox.getFruit();
        System.out.println(banana);
    }
}
