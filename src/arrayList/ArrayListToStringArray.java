package arrayList;

import java.util.ArrayList;

public class ArrayListToStringArray {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>() {{
            add("ala");
            add("ma");
            add("kota");
        }};

        String[] array = list.toArray(new String[list.size()]);

        for (String str : array) {
            System.out.println(str);
        }
    }


}
