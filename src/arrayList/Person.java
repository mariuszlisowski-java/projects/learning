package arrayList;

import java.util.ArrayList;
import java.util.Arrays;

public class Person {
    private String name;

    public Person(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person's name: " + name;
    }

    public static void main(String[] args) {
        // array
        Person[] persons = new Person[2];
        persons[0] = new Person("Alice");
        persons[1] = new Person("Emma");

        System.out.println(Arrays.toString(persons)); // all array's objects

        // ArrayList
        ArrayList<Person> couples = new ArrayList<Person>();
        Person wife = new Person("Emma");
        Person husband = new Person("Robert");

        couples.add(wife);
        couples.add(husband);
        int wifeIndex = couples.indexOf(wife);
        int husbandIndex = couples.indexOf(husband);

        System.out.println(couples.toString()); // all array's objects
        System.out.println(couples.get(wifeIndex)); // object
        System.out.println(couples.get(husbandIndex)); // object
        System.out.println(couples.contains(wife)); // boolean

        Person newWife = new Person("Rebeca");
        Person anotherWife = new Person("Kate");
        couples.set(0, newWife); // overwrites at index 0
        couples.add(0, anotherWife); // add at index 0

        System.out.println(couples.toString()); // no need to use .toString()

        // new array initialization
        Person[] women = {wife, husband, newWife, anotherWife};
        // converting an array to an ArrayList
        ArrayList<Person> womenList = new ArrayList<>(Arrays.asList(women));
        // converting as ArrayList to an array
        Person[] oldWomen = womenList.toArray(new Person[0]);

        System.out.println(Arrays.toString(women));
        System.out.println(Arrays.toString(oldWomen));
        System.out.println(womenList.toString());
        System.out.println(womenList); // same output as above

        // length & size
        System.out.println(women.length); // array
        System.out.println(womenList.size()); // ArrayList

        // initial capacity
        ArrayList<Person> people = new ArrayList<>(4);

        // clearing an ArrayList
        womenList.clear();
    }

}
