package arrayList;

import java.util.ArrayList;
import java.util.List;

public class RemoveUsingCopy {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>() {{
            add("ala");
            add("ma");
            add("kota");
        }};

        System.out.println(list);
        removeEntry(list, "ma");
        System.out.println(list);
    }

    public static void removeEntry(List<String> list, String entry) {
        // working on a copy
        for (String el : new ArrayList<>(list)) {
            if (entry != null & entry.equals(el)) {
                list.remove(el); // remove from original
            }
        }
    }

}
