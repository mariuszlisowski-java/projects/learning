package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ReverseList {

    public static void main(java.lang.String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7));

        Collections.reverse(numbers);
        System.out.println(numbers);
    }

}
