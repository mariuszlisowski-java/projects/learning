package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Identical {
    public static void main(java.lang.String[] args) {
        String mercury = new String("Mercury");
        String venus = new String("Venus");
        String earth = new String("Earth");
        String mars = new String("Mars");
        String jupiter = new String("Jupiter");
        String saturn = new String("Saturn");
        String uranus = new String("Uranus");
        String neptune = new String("Neptune");

        ArrayList<String> solarSystemPart1 = new ArrayList<>(Arrays.asList(mercury, venus, earth, mars));
        ArrayList<String> solarSystemPart2 = new ArrayList<>(Arrays.asList(jupiter, saturn, uranus, neptune));

        // are disjointed (do they have at least one identical element)? NO hence true
        System.out.println(Collections.disjoint(solarSystemPart1, solarSystemPart2));

    }
}
