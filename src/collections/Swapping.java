package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Swapping {
    public static void main(java.lang.String[] args) {
        String mercury = new String("Mercury");
        String venus = new String("Venus");
        String earth = new String("Earth");
        String mars = new String("Mars");
        String jupiter = new String("Jupiter");
        String saturn = new String("Saturn");
        String uranus = new String("Uranus");
        String neptune = new String("Neptune");

        ArrayList<String> solarSystem = new ArrayList<>(Arrays.asList(neptune, venus, earth, mars
                , jupiter, saturn, uranus, mercury)); // the planets are in the wrong order
        System.out.println(solarSystem);

        // works on indices hence .indexOf
        Collections.swap(solarSystem, solarSystem.indexOf(mercury), solarSystem.indexOf(neptune));
        System.out.println(solarSystem);

    }
}
