package collections;

import java.util.ArrayList;
import java.util.Collections;

public class Shuffle {
    public static void main(java.lang.String[] args) {
        ArrayList<Integer> array = new ArrayList<>(100);
        for (int i = 0; i < 10; i++) {
            array.add(i); // add the numbers 0 to 9 in sequence
        }
        printArray(array);

        Collections.shuffle(array); // mix it up
        printArray(array);
    }
    public static void printArray(ArrayList<Integer> array) {
        for (Integer el : array) {
            System.out.print(el + " ");
        }
        System.out.println();
    }

}
