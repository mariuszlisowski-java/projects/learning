package collections;

import java.util.Collections;

import java.util.ArrayList;
import java.util.Arrays;

public class SortingMinMax {
    public static void main(java.lang.String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7));

        System.out.println(Collections.max(numbers));
        System.out.println(Collections.min(numbers));
    }

}
