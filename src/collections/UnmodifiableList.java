package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collections;

public class UnmodifiableList {
    public static void main(java.lang.String[] args) {

        String mercury = new String("Mercury");
        String venus = new String("Venus");
        String earth = new String("Earth");
        String mars = new String("Mars");
        String jupiter = new String("Jupiter");
        String saturn = new String("Saturn");
        String uranus = new String("Uranus");
        String neptune = new String("Neptune");

        List<String> solarSystemA = Collections.unmodifiableList(new ArrayList<>(Arrays.asList(
                mercury, venus, earth, mars, jupiter, saturn, uranus, neptune)));
        // or
        List<String> solarSystemB = List.of(mercury, venus, earth, mars, jupiter, saturn, uranus, neptune);

//         solarSystemA.add("Pluto"); // ERROR
//         solarSystemB.add("Pluto"); // ERROR
    }
}
