package collections;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ReverseArray {

    public static void main(java.lang.String[] args) {
//        byte[] numbers = new byte[] {1, 2, 3, 4, 5, 6, 7};
        int[] numbers = new int[] {1, 2, 3, 4, 5, 6, 7};


        // convert primitives to objects
        Integer[] array = Arrays.stream(numbers)
                .boxed()
                .toArray(Integer[]::new);
        Collections.reverse(Arrays.asList(array));
        // or
        List<Integer> list = Arrays.stream(numbers)
                .boxed()
                .collect(Collectors.toList());
        Collections.reverse(list);

        printReversedArray(array);
        printReversedList(list);
    }

    private static void printReversedList(List<Integer> list) {
        for (Integer value : list) {
            System.out.print(value + " ");
        }
        System.out.println();
    }

    private static void printReversedArray(Integer[] array) {
        for (Integer value : array) {
            System.out.print(value + " ");
        }
        System.out.println();
    }

}
