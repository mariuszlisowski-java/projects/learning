package collections;

public class ReversePrimitive {

    public static void main(String[] args) {
        byte[] numbers = new byte[] {1, 2, 3, 4, 5, 6, 7};

        reverse(numbers);

        for (byte value : numbers) {
            System.out.print(value + " ");
        }
    }

    public static void reverse(byte[] array) {
        for (int i = 0; i < array.length / 2; i++) {        // 7 / 2 = 3 loops
            byte temp = array[i];
            // verbose
            System.out.print("swapping: " + temp + " : ");
            array[i] = array[array.length - i - 1];
            // verbose
            System.out.println(array[i]);
            array[array.length - i - 1] = temp;
        }
    }

}
