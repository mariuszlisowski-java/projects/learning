package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Initialization {
    public static void main(String[] args) {
        // method 1
        ArrayList<Integer> myList = new ArrayList<>();
        Collections.addAll(myList, 1, 2, 3, 4, 5, 6, 7, 8, 9);

        // method 2
        ArrayList<Integer> myCopiedList = new ArrayList<>();
        myCopiedList.addAll(myList);

        // method 3
        List<Integer> myInts = new ArrayList<>();
        myInts = Arrays.asList(9, 8, 7, 6, 5, 4, 3, 2, 1);
        
        printList(myList);
        printList(myCopiedList);
        printList(myInts);
    }

    public static void printList(List<Integer> lista) {
        for (Integer el : lista) {
            System.out.print(el + " ");
        }
        System.out.println("\n-----------------");
    }

}
