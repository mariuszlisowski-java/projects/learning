package lambda.functional;

import java.io.PrintWriter;
import java.util.function.Consumer;

public class App {
    public static void main(String[] args) {
        // custom functional interface
        // implemented with lambda
        IStringPrinter printerA =
                str -> System.out.println(str);
        testStringPrinter(printerA);
        // or with anonymous object
        IStringPrinter printerB = new IStringPrinter() {
            @Override
            public void printString(String str) {
                System.out.println(str);
            }
        };
        testStringPrinter(printerB);

        // Consumer functional interface
        // implemented with lambda
        testConsumer(str -> System.out.println(str));
        // or with external class
        PrintText textPrinter = new PrintText();
        testConsumer(textPrinter::printText);
    }

    // custom
    public static void testStringPrinter(IStringPrinter printer) {
        printer.printString("Using IStringPrinter");
    }
    // Consumer
    public static void testConsumer(Consumer<String> consumer) {
        consumer.accept("Using Consumer");
    }

}