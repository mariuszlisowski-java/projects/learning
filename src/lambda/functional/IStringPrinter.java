package lambda.functional;

@FunctionalInterface
public interface IStringPrinter {
    void printString(String str);
}
