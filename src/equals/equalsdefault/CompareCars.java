package equals.equalsdefault;

public class CompareCars {
    public static void main(String[] args) {
        CarDefaultEquals bugattiA = new CarDefaultEquals("Veyron", 389);
        CarDefaultEquals bugattiB = new CarDefaultEquals("Veyron", 389);

        // comparison of references (which are different)
        System.out.println(bugattiA == bugattiB);           // false
        // default equals implementation (not enough here)
        System.out.println(bugattiA.equals(bugattiB));      // false
    }
}
