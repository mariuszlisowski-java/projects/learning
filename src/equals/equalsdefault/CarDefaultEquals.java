package equals.equalsdefault;

public class CarDefaultEquals {
    String model;
    int maxSpeed;

    public CarDefaultEquals(String model, int maxSpeed) {
        this.model = model;
        this.maxSpeed = maxSpeed;
    }

    // default equals implementation used here
    public boolean equals(Object obj) {
        return (this == obj);
    }
}
