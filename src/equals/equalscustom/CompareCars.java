package equals.equalscustom;

public class CompareCars {
    public static void main(String[] args) {
        CarCustomEquals bugattiA = new CarCustomEquals("Veyron", 389);
        CarCustomEquals bugattiB = new CarCustomEquals("Veyron", 389);

        // comparison of references (which are different)
        System.out.println(bugattiA == bugattiB);           // false
        // custom equals implementation (works here well)
        System.out.println(bugattiA.equals(bugattiB));      // true
    }

}
