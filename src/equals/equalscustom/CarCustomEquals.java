package equals.equalscustom;

import java.util.Objects;

public class CarCustomEquals {
    String model;
    int maxSpeed;

    public CarCustomEquals(String model, int maxSpeed) {
        this.model = model;
        this.maxSpeed = maxSpeed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarCustomEquals that = (CarCustomEquals) o;
        return maxSpeed == that.maxSpeed &&
                Objects.equals(model, that.model);
    }
}
