package equals.intcompare;

public class Main {
    public static void main(String[] args) {
        // properly implemented equals
        ManProper manA = new ManProper();
        ManProper manB = new ManProper();
        manA.dnaCode = 1234;
        manB.dnaCode = 1234;
        System.out.println(manA.equals(manB)); // true (OK)

        // equals not implemented (default used)
        ManFaulty manC = new ManFaulty();
        ManFaulty manD = new ManFaulty();
        manC.dnaCode = 5678;
        manD.dnaCode = 5678;
        System.out.println(manC.equals(manD)); // false (should be true)

    }
}
