package equals.intcompare;

public class ManFaulty {
    int dnaCode;

    // faulty default implementation (pasted here)
    public boolean equals(Object obj) {
        return (this == obj);
    }
}
