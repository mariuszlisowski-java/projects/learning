package equals.stringscompare;

public class Main {
    public static void main(String[] args) {
        // '==' operator compares references (to string pool)
        String strA = "abc";
        String strB = "abc"; // refers to the same address as strA
        System.out.println(strA == strB); // true (same addresses)

        String strC = new String("abc"); // refers to different address
        System.out.println(strA == strC);       // false

        // 'equals' method compares the sequence of characters
        System.out.println(strA.equals(strC));  // true (same contents)

        // 'intern' method checks matching string in the string pool
        // and returns the reference to the string in the pool
        System.out.println(strA == strC.intern());  // true (found content of strC in pool)
    }
}
