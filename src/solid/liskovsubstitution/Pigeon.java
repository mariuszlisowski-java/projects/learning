package solid.liskovsubstitution;

public class Pigeon extends Animal {
    public Pigeon() {
        super("Pigeon");
    }

    @Override
    public Legs legCount() {
        return Legs.TWO;
    }
}
