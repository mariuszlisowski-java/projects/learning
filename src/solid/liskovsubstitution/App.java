package solid.liskovsubstitution;

import java.util.LinkedList;
import java.util.List;

/*
    A sub-class must be substitutable for its super-class.
    The aim of this principle is to ascertain that a sub-class can assume the place of its super-class without errors.
    If the code finds itself checking the type of class then, it must have violated this principle.
 */

public class App {

    public static void main(String[] args) {
        List<Animal> animals = new LinkedList<>() {{
            add(new Pigeon());
            add(new Cat());
            add(new Spider());
        }};

        animals.stream()
                .forEach(animal ->
                        System.out.println(animal.getName() + " has " + animal.legCount().getCount() + " legs"));
    }

}
