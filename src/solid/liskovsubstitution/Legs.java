package solid.liskovsubstitution;

public enum Legs {
    TWO("two"), FOUR("four"), SIX("six");

    private final String count;

    Legs(String count) {
        this.count = count;
    }

    public String getCount() {
        return count;
    }
}
