package solid.liskovsubstitution;

public class Spider extends Animal {
    public Spider() {
        super("Spider");
    }

    @Override
    public Legs legCount() {
        return Legs.SIX;
    }
}
