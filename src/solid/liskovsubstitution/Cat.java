package solid.liskovsubstitution;

public class Cat extends Animal {
    public Cat() {
        super("Cat");
    }

    @Override
    public Legs legCount() {
        return Legs.FOUR;
    }
}
