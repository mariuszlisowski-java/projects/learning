package solid.dependencyinversion;

/*
    Dependency should be on abstractions not concretions
    A. High-level modules should not depend upon low-level modules. Both should depend upon abstractions.
    B. Abstractions should not depend on details. Details should depend upon abstractions.
 */

public class App {

    public static void main(String[] args) {
        HttpService httpService = new HttpService();

        Http realHttp = new Http(httpService);
        realHttp.get("http://java.io/home");

        MockHttpService mockService = new MockHttpService();
        Http mockHttp = new Http(mockService);
        mockHttp.get("https://cpp.net/home");
    }

}
