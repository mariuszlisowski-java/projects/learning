package solid.dependencyinversion;

public class HttpService implements Connection {
    @Override
    public void request(String url) {
        System.out.println("Requesting url: " + url);
    }
}
