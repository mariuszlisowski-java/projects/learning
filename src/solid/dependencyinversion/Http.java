package solid.dependencyinversion;

public class Http {
    private final Connection httpConnection;

    // class should care less the type of Http service used
    public Http(Connection connection) {
        this.httpConnection = connection;
    }

    // using interface method inside
    public void get(String url) {
        this.httpConnection.request(url);
    }
}
