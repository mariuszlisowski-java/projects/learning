package solid.dependencyinversion;

public interface Connection {
    void request(String url);
}
