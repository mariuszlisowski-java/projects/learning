package solid.dependencyinversion;

public class MockHttpService implements Connection  {
    @Override
    public void request(String url) {
        System.out.println("Requesting mock: " + url);
    }
}
