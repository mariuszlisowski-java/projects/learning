package solid.singleresponsibility;

public class Animal {
    private final String name;

    Animal(String name) {
        this.name = name;
    }

    public String getAnimalName() {
        return name;
    }

    // SRP violated
    // public Animal getAnimal() {
    //     return this;
    // }

    // SRP violated
    // public void saveAnimal() {
    //     System.out.println("animal saved in database...");
    // }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                '}';
    }
}
