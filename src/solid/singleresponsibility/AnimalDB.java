package solid.singleresponsibility;

public class AnimalDB {
    Animal animal;

    // SRP conform
     public Animal getAnimal() {
         return animal;
     }

    // SRP conform
     public void saveAnimal(Animal animal) {
         this.animal = animal;
         System.out.println(animal.getAnimalName() + " saved in a database...");
     }
}
