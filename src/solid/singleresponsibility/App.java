package solid.singleresponsibility;

/*
    A class should have only one job.
    A class should be responsible for only one thing. If a class has more than one responsibility,
    it becomes coupled. A change to one responsibility results to modification of the other responsibility.
 */

public class App {

    public static void main(String[] args) {
        Animal mammal = new Animal("Mammal");

        AnimalDB dataBase = new AnimalDB();
        dataBase.saveAnimal(mammal);
        System.out.println(dataBase.getAnimal());
    }

}
