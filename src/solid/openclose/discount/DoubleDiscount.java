package solid.openclose.discount;

public class DoubleDiscount extends Discount {
    public DoubleDiscount(Item item) {
        super(item);
    }

    public float giveDiscount() {
        return super.giveDiscount() * 2;
    }
}
