package solid.openclose.discount;

public class TripleDiscount extends Discount {
    public TripleDiscount(Item item) {
        super(item);
    }

    public float giveDiscount() {
        return super.giveDiscount() * 3;
    }
}
