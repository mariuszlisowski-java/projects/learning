package solid.openclose.discount;

public class Discount {
    final static float DISCOUNT = 0.2F;
    Item item;

    public Discount(Item item) {
        this.item = item;
    }
    public float giveDiscount() {
        return item.getPrice() * DISCOUNT;
    }
}
