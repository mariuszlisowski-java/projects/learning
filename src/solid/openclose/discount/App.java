package solid.openclose.discount;

public class App {

    public static void main(String[] args) {
        Item book = new Item("Java", 100F);

        // base class closed for modifications
        Discount discount = new Discount(book);
        System.out.println(discount.giveDiscount());

        // but open for extensions
        Discount doubleDiscount = new DoubleDiscount(book);
        System.out.println(doubleDiscount.giveDiscount());

        Discount tripleDiscount = new TripleDiscount(book);
        System.out.println(tripleDiscount.giveDiscount());
    }

}
