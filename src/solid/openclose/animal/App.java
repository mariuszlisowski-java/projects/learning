package solid.openclose.animal;

import java.util.ArrayList;
import java.util.List;

/*
    Software entities (classes, modules, functions) should be open for extension but closed for not modification.
 */

public class App {
    public static void main(String[] args) {
        // app open for extensions
        List<Animal> animals = new ArrayList<>() {{
            add(new Lion());
            add(new Snake()); // let's extend app adding snake
            add(new Mouse()); // let's extend app adding mouse
        }};

        // function closed for modifications
        animals.stream()
                .forEach(Animal::makeSound);
    }

}
