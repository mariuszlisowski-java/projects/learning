package solid.openclose.animal;

public class Mouse extends Animal {
    Mouse() {
        super("Mouse");
    }

    @Override
    public void makeSound() {
        System.out.println("squeak...");
    }
}
