package solid.openclose.animal;

public class Lion extends Animal {
    public Lion() {
        super("Lion");
    }

    @Override
    public void makeSound() {
        System.out.println("roar...");
    }
}
