package solid.openclose.animal;

public class Snake extends Animal {
    public Snake() {
        super("Snake");
    }

    @Override
    public void makeSound() {
        System.out.println("hiss...");
    }
}
