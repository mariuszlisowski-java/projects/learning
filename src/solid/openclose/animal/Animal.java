package solid.openclose.animal;

public abstract class Animal {
    private final String name;

    Animal(String name) {
        this.name = name;
    }

    public String getAnimalName() {
        return name;
    }

    public abstract void makeSound();
}
