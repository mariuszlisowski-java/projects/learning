package solid.interfacesegregation.incorrect;

public class Rectangle implements IShape {
    @Override
    public void drawRectangle() {}

    // not used
    @Override
    public void drawCircle() {}
    // not used
    @Override
    public void drawSquare() {}

}
