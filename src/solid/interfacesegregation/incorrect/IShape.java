package solid.interfacesegregation.incorrect;

public interface IShape {
    // not segregated
    void drawCircle();
    void drawSquare();
    void drawRectangle();
}
