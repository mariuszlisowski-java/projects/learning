package solid.interfacesegregation.incorrect;

public class App {

    public static void main(String[] args) {
        Circle circle = new Circle();

        circle.drawCircle();
        circle.drawSquare();        // illogical
        circle.drawRectangle();     // illogical
    }

}
