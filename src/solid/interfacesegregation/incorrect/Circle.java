package solid.interfacesegregation.incorrect;

public class Circle implements IShape {
    @Override
    public void drawCircle() {}

    // not used∂
    @Override
    public void drawSquare() {}
    // not used∂
    @Override
    public void drawRectangle() {}
}
