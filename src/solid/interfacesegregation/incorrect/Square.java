package solid.interfacesegregation.incorrect;

public class Square implements IShape {
    @Override
    public void drawSquare() {}

    // not used
    @Override
    public void drawCircle() {}
    // not used
    @Override
    public void drawRectangle() {}
}
