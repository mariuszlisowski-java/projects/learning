package solid.interfacesegregation.correct;

/*
    Make fine grained interfaces that are client specific.
    Clients should not be forced to depend upon interfaces that they do not use.
 */

public class App {

    public static void main(String[] args) {
        Circle circle = new Circle();
        Square square = new Square();
        Rectangle rectangle = new Rectangle();
        CustomShape customShape = new CustomShape();

        circle.drawCircle();
        square.drawSquare();
        rectangle.drawRectangle();
        customShape.draw();
    }

}
