package solid.interfacesegregation.correct;

public interface ICircle {
    void drawCircle();
}
