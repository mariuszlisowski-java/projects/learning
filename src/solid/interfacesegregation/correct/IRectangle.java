package solid.interfacesegregation.correct;

public interface IRectangle {
    void drawRectangle();
}
