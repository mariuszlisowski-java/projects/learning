package solid.interfacesegregation.correct;

public class CustomShape implements IShape {
    @Override
    public void draw() {
        System.out.println("Drawing custom shape...");
    }
}
