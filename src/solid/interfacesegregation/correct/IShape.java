package solid.interfacesegregation.correct;

public interface IShape {
    void draw();
}
