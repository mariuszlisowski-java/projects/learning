package solid.interfacesegregation.correct;

public class Rectangle implements IRectangle  {
    @Override
    public void drawRectangle() {
        System.out.println("Drawing rectangle...");
    }
}
