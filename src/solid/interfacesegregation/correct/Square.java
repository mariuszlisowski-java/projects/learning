package solid.interfacesegregation.correct;

public class Square implements ISquare {
    @Override
    public void drawSquare() {
        System.out.println("Drawing square...");
    }
}
