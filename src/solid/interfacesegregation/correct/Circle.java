package solid.interfacesegregation.correct;

public class Circle implements ICircle {
    @Override
    public void drawCircle() {
        System.out.println("Drawing circle...");
    }
}
