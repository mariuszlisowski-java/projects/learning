package solid.interfacesegregation.correct;

public interface ISquare {
    void drawSquare();
}
