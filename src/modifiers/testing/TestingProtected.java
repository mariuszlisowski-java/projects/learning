package modifiers.testing;

// no inheritance
public class TestingProtected {
    public void someTest() {
        Base base = new Base();
        base.doNothing(); // protected method within the same package access OK
    }
}
