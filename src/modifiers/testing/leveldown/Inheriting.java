package modifiers.testing.leveldown;

import modifiers.testing.Base;

// inherited (different package)
public class Inheriting extends Base {
    public void someTest() {
        Base base = new Base();
        // base.doNothing(); // direct access to protected method denied

        Inheriting inheriting = new Inheriting();
        inheriting.doNothing(); // indirect access OK
    }

}
