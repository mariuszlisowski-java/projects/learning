package modifiers.testing;

// inherited (same package)
public class Inheriting extends Base {
    protected void someMethod() {
        doNothing(); // protected base method & the same package access OK
    }
}
