package modifiers.access;

public class TestA {
    public void methodPublic(){ methodPrivate(); }
    protected void methodProtected(){ methodPrivate();}
    void methodDefault(){ methodPrivate(); }

    private void methodPrivate(){}
}
