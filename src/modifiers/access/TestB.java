package modifiers.access;

public class TestB {
    public static void main(String[] args) {
        new TestA().methodPublic();
        new TestA().methodProtected();
        new TestA().methodDefault();
        // new TestA().methodPrivate() // not accessible because is PRIVATE
    }

    public void methodPublic() {}
    protected void methodProtected() {}
    void methodDefault() {}
    private void methodPrivate() {}
}
