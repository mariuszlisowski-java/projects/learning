package modifiers.access.util;

import modifiers.access.TestB;

// inheritance
public class TestE extends TestB {
    public static void main(String[] args) {
        new TestB().methodPublic();
        new TestE().methodProtected(); // accessing superclass protected method using subclass
        // new TestB().methodProtected(); // accessing superclass protected method directly
    }
}
