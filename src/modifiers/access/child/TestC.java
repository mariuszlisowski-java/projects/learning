package modifiers.access.child;

import modifiers.access.TestB;

public class TestC {
    public static void main(String[] args) {
        new TestB().methodPublic(); // accessible because is PUBLIC
        // new TestB().methodProtected() {}
        // new TestB().methodDefault() {}
        // new TestB().methodPrivate() {}
    }
}
