package array;

import java.util.Arrays;
import java.util.Collections;

public class SortInteger {
    public static void main(String[] args) {
        Integer[] array = new Integer[] { 8, 16, 32, 64, 128, 256 };

        // descending
        Arrays.sort(array, Collections.reverseOrder());
        System.out.println(Arrays.toString(array));

        // ascending
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
    }

}
