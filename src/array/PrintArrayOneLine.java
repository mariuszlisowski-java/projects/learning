package array;

import java.util.Arrays;

public class PrintArrayOneLine {
    public static void main(String[] args) {
        int[] array = {5, 7, 9, 2, 1};

        System.out.println(Arrays.toString(array));
    }

}
