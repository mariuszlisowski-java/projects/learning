package array;

import java.util.stream.IntStream;

public class ArrayIntegerGenerator {
    final static int RANGE_MIN = 1996; // stop programming
    final static int RANGE_MAX = 2021; // start programming

    public static void main(String[] args) {
        // generate int range and convert to Integer
        Integer[] years = IntStream.range(RANGE_MIN, RANGE_MAX) // open range
                .boxed()
                .toArray(Integer[]::new);

        int counter = 0;
        for (Integer year : years) {
            System.out.print(year + ", ");;
            ++counter;
        }
        System.out.println("\nYears passed: " + counter);
    }

}
