package array;

import java.util.Arrays;
import java.util.Collections;

public class SortInt {
    public static void main(String[] args) {
        int[] array = { 5, 7, 9, 2, 1 };

        arraySortAscending(array);
        System.out.println(Arrays.toString(array));

        int[] descending = arraySortDescendingStreamBoxed(array);
        System.out.println(Arrays.toString(descending));
    }

    public static void arraySortAscending(int[] array) {
        Arrays.sort(array);
    }

    public static int[] arraySortDescendingStreamBoxed(int[] array) {
        return Arrays.stream(array).boxed()
                     .sorted(Collections.reverseOrder())
                     .mapToInt(Integer::intValue)
                     .toArray();
    }

}
