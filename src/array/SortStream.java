package array;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class SortStream {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] array = new int[] {64,74,23,67,99,76,44,22,44,67,88,98,10};

        sort(array);
        Arrays.stream(array).forEach(System.out::println);
    }

    public static void sort(int[] array) {
        List<Integer> collected = Arrays.stream(array)
                .boxed()
                .sorted()
                .collect(Collectors.toList());

        replaceArrayValuesAtomic(collected, array);
    }

    public static void replaceArrayValuesAtomic(List<Integer> collected, int[] array) {
        AtomicInteger ordinal = new AtomicInteger(0);
        collected.forEach(integer -> {
            array[ordinal.getAndIncrement()] = integer;
        });
    }

    public static void replaceArrayValues(List<Integer> collected, int[] array) {
        int ordinal = 0;
        for (Integer integer : collected) {
            array[ordinal++] = integer;
        }
    }

}
