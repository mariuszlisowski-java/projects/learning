package array;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortMixedArrayString {
    public static void main(String[] args) throws Exception {
        // sort words alphabetically and numbers descending
        ArrayList<String> mixedArray = new ArrayList<String>() {{
            add("City");
            add("1");
            add("Bar");
            add("3");
            add("Amor");
            add("9");
            add("0");
            add("Zeta");
        }};

        String[] tablica = mixedArray.toArray(new String[mixedArray.size()]);
        sortMixedArray(tablica);

        // sorted array
        for (String x : tablica) {
            System.out.println(x);
        }
    }

    public static void sortMixedArray(String[] array) {
        // copy words and numbers to separate lists
        List<Integer> numbers = new ArrayList<>();
        List<String> words = new ArrayList<>();
        for (String str : array) {
            // is string a number
            if (str.matches("-?\\d+"))
                // -?     --> negative sign, could have none or one
                // \\d+   --> one or more digits
                numbers.add(Integer.parseInt(str));
            else
                words.add(str);
        }
        // sort numbers
        Collections.sort(numbers, Collections.reverseOrder());
        // sort words (bubble sort)
        for (int i = 0; i < words.size(); i++) {
            for (int j = 0; j < words.size() - 1; j++) {
                String pom;
                if (words.get(j).compareTo(words.get(j + 1)) > 0) {
                    // swap
                    pom = words.get(j);
                    words.set(j, words.get(j + 1));
                    words.set(j + 1, pom);
                }
            }
        }

        // replace indexes of array (not the array as it is a reference)
        int counterNums = 0;
        int counterWords = 0;
        for (int i = 0; i < array.length; i ++) {
            // is string a number
            if (array[i].matches("-?\\d+")) {
                array[i] = Integer.toString(numbers.get(counterNums++));
            } else {
                array[i] = words.get(counterWords++);
            }
        }
    }


}
