package array;

import com.sun.jdi.ArrayReference;

import java.util.Arrays;
import java.util.Collections;

public class SortReverseArray {
    public static void main(String[] args) throws Exception {
        int[] array = new int[] { 2, 4, 6, 7, 3, 8, 6 ,9, 1};

        sort(array);
        printArray(array);
    }

    public static void sort(int[] array) {
        int[] sortedArray = Arrays.stream(array).boxed()
                .sorted(Collections.reverseOrder())
                .mapToInt(Integer::intValue)
                .toArray();
        // replace argument array with sorted one
        for (int i = 0; i < array.length; i++) {
            array[i] = sortedArray[i];
        }
    }

    public static void copyArray(int[] array) {
    }

    private static void printArray(int[] array) {
        System.out.println(Arrays.toString(array));
    }
}
