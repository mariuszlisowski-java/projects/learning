package array;

import java.util.Arrays;

public class ArrayMinMax {
    public static void main(String[] args){
        int[] tab = {12, 1, 21, 8};

        int min = Arrays.stream(tab).min().getAsInt();
        int max = Arrays.stream(tab).max().getAsInt();

        System.out.println("Min: " + min);
        System.out.println("Max: " + max);
    }
}
