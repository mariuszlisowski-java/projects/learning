package random;

import java.util.Map;

public class CastToIntMultiply {
    public static void main(String[] args) {
        for (int i = 0; i < 20; i++) {
            int random = getRandomNumber(9); // 0 - 9
            System.out.print(random + " ");
        }
    }

    public static int getRandomNumber(int maximum) {
        return (int) (Math.random() * (maximum + 1)); // * 10, max 9.99999
    }
}
