package map;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class MapKeySortedReversed {
    // descending order by key
    static Map<Integer, Integer> counts = new TreeMap<>(Collections.reverseOrder());

    static int[] array = {1, 1, 5, 6, 3, 5, 9, 0, 8, 8, 9};

    public static void main(String[] args) {
        // count occurrences of numbers in an array
        for (int value : array) {
            counts.merge(value, 1, Integer::sum); // if key do not exists, put 1 as value
                                                        // otherwise sum 1 to the value linked to key
        }

        for (Map.Entry<Integer, Integer> pair : counts.entrySet()) {
            System.out.println("Number " + pair.getKey() + " occurs " + pair.getValue() + " time(s)");
        }

    }
}
