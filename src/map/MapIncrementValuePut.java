package map;

import java.util.Map;
import java.util.TreeMap;

public class MapIncrementValuePut {
    static Map<Integer, Integer> counts = new TreeMap<>(); // sorted map

    static int[] array = {1, 1, 5, 6, 3, 5, 9, 0, 8, 8, 9};

    public static void main(String[] args) {
        // count occurrences of numbers in an array
        for (int value : array) {
            // if no such a key, default is 0 incremented by 1
            counts.put(value, counts.getOrDefault(value, 0) + 1); //
        }

        for (Map.Entry<Integer, Integer> pair : counts.entrySet()) {
            System.out.println("Number " + pair.getKey() + " occurs " + pair.getValue() + " time(s)");
        }

    }
}
