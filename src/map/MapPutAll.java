package map;

import java.util.HashMap;
import java.util.Map;

public class MapPutAll {
    public static void main(String[] args) {
        HashMap<Integer, String> mapA = new HashMap<>();
        mapA.put(212133, "Ivo");
        mapA.put(162348, "Ted");
        mapA.put(808271, "Tom");

        HashMap<Integer, String> mapB = new HashMap<>();
        mapB.put(312131, "Ana");
        mapB.put(862343, "Ele");
        mapB.put(608275, "Ali");

        mapA.putAll(mapB); // combine two maps into A
        
        for (Map.Entry<Integer, String> pair : mapA.entrySet() ) {
            System.out.println(pair);
        }
        // or
        System.out.println(mapA);

    }

}
