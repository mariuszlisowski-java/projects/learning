package map;

import java.util.HashMap;
import java.util.Map;

public class MapRemoveValWays {
    public static HashMap<String, String> createMap() {
        HashMap<String, String> names = new HashMap<>();

        names.put("A", "John");
        names.put("B", "Tom");
        names.put("C", "Alice");

        return names;
    }

    public static void removeForValueFromMap(Map<String, String> map, String value) {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        // cannot remove while iterating thus copy used
        for (Map.Entry<String, String> para : copy.entrySet()) {
            if (para.getValue().equals(value))
                map.remove(para.getKey());
        }
    }
    // same as above
    public static void removeForValueFromMapIf(Map<String, String> map, String value) {
        map.entrySet().removeIf(entry -> entry.getValue().equals(value));
    }

    public static void main(String[] args) {
        HashMap<String, String> map = createMap();
        removeForValueFromMapIf(map, "Alice");
        for (String surname : map.keySet()) {
            System.out.println(surname);
        }
    }

}
