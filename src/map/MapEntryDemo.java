package map;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MapEntryDemo {
    public static Map<String, String> createRandomMap() {
        Map<String, String> randomMap = new HashMap<>();
        StringBuilder strA = new StringBuilder();
        StringBuilder strB = new StringBuilder();
        Random random = new Random();

        // map of 5 entries
        for (int i = 0; i < 5; i++) {
            // generate 5 letter long word twice
            for (int j = 0; j < 5; j++) {
                char charA = (char) (random.nextInt(25) + 97); // a-z [97, 122]
                char charB = (char) (random.nextInt(25) + 97); // a-z [97, 122]
                strA.append(Character.toString(charA));
                strB.append(Character.toString(charB));
            }
            // put into a map
            randomMap.put(strA.toString(), strB.toString());
            // clear strings
            strA = new StringBuilder();
            strB = new StringBuilder();
        }

        return randomMap;
    }

    // map entry
    private static void getMap(Map<String, String> names) {
        for (HashMap.Entry<String, String> pair : names.entrySet()) {
            System.out.println(pair.getKey() + " " + pair.getValue());
        }
    }

    // keys entry
    public static void getKeys(Map<String, String> map) {
        for (String key : map.keySet()) {
            System.out.print(key + " ");
        }
        System.out.println();
    }

    // values entry
    public static void getValues(Map<String, String> map) {
        for (String value : map.values()) {
            System.out.print(value + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Map<String, String> map = createRandomMap();

        getMap(map);
        getKeys(map);
        getValues(map);
    }

}