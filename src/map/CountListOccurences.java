package map;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CountListOccurences {
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<String>(
                Arrays.asList("ala", "ma", "kota", "i", "ela", "ma", "kota", ""));

        Map<String, Integer> map = countWords(words);
        map.forEach((key, value) -> System.out.println(value + " occurrence(s) of " + key));
    }

    public static Map<String, Integer> countWords(ArrayList<String> list) {
        HashMap<String, Integer> result = new HashMap<String, Integer>();

        for (String string : list) {
            if (!string.isEmpty()) {
                result.merge(string, 1, Integer::sum); // increment by 1
            }
        }

        return result;
    }
}
