package map;

import java.util.HashMap;
import java.util.Map;

public class MapAllDuplicateValRemove {
    public static HashMap<String, String> createMap() {
        HashMap<String, String> names = new HashMap<>();

        names.put("A", "John");
        names.put("B", "Tom");
        names.put("C", "John");
        names.put("D", "John");
        names.put("E", "John");
        names.put("F", "Tom");
        names.put("G", "John");
        names.put("H", "John");
        names.put("I", "John");
        names.put("J", "Alice"); // the only not duplicated

        return names;
    }

    // removes duplicated values leaving none
    public static void removeAllDuplicatedValues(Map<String, String> mapa) {
        HashMap<String, String> copy = new HashMap<String, String>(mapa);

        for (Map.Entry<String, String> pairA : copy.entrySet()) {
            for (Map.Entry<String, String> pairB : copy.entrySet()) {
                String wartosc = pairB.getValue();
                if (pairA.getValue().equals(wartosc) && !pairA.getKey().equals(pairB.getKey())) {
                    mapa.entrySet().removeIf(entry -> entry.getValue().equals(pairB.getValue()));
                }
            }
        }
    }

    public static void main(String[] args) {
        HashMap<String, String> map = createMap();
        removeAllDuplicatedValues(map);
        for (String surname : map.keySet()) {
            System.out.println(surname);
        }
    }

}
