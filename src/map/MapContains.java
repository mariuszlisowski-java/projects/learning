package map;

import java.util.HashMap;

public class MapContains {
    public static void main(String[] args) {
        HashMap<Integer, String> map = new HashMap<>();

        map.put(212133, "Ivo");
        map.put(162348, "Ted");
        map.put(808271, "Tom");

        if (map.containsValue("Ivo")) {
            System.out.println("Value found");
        }
        if (map.containsKey(212133)) {
            System.out.println("Key found! It's value: " + map.get(212133));
        }
    }

}
