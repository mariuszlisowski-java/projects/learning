package map;

import java.text.Collator;
import java.util.*;

public class TreeMapComparatorChar {
    public static void main(String[] args) {
        // polish alphabet
        String aplhabet = "aąbcćdeęfghijklłmnńoóprsśtuwyzźż";
        // convert to list
        List<Character> lettersList = new ArrayList<>();
        for (char litera : aplhabet.toCharArray()) {
            lettersList.add(litera);
        }
        // example list to count occurrences
        ArrayList<String> sentences = new ArrayList<String>() {{
            add("ż");
            add("z");
            add("ąą");
            add("aa");
        }};

        // character overridden Comparator using polish locale
        Comparator<Character> polishComparator = new Comparator<Character>() {
            private Collator collator = Collator.getInstance(new Locale("pl", "PL"));
            @Override
            public int compare(Character ch1, Character ch2) {
                return collator.compare(ch1.toString(), ch2.toString());
            }
        };

        // count occurrences of each letter in all sentences
        Map<Character, Long> lettersNumber = new java.util.TreeMap<>(polishComparator);
        for (String string : sentences) {
            lettersList.forEach(character -> {
                        long count = string.chars()
                                .filter(ch -> ch == character)
                                .count();
                        if (count != 0) {
                            lettersNumber.merge(character, count, Long::sum);
                        }
                    });
        }
        // print results (already in alphabetic order)
        for (Map.Entry<Character, Long> pair : lettersNumber.entrySet()) {
            System.out.println(pair.getKey() + " " + pair.getValue());
        }

    }



}
