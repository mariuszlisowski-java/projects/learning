package map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class MapSetValues {
    public static void main(String[] args) {
        HashMap<Integer, String> map = new HashMap<>();

        map.put(212133, "Ivo");
        map.put(162348, "Ted");
        map.put(808271, "Tom");

        Set keys = map.keySet();
        System.out.println(keys);

        List<String> str = new ArrayList<>(map.values());
        System.out.println(str);
    }

}
