package map;

import java.util.*;

public class MapIteratorDemo {
    public static HashMap<String, Integer> createMap() {
        HashMap<String, Integer> map = new HashMap<>();
        map.put("Ola", 1);
        map.put("Ula", 2);
        map.put("Ala", 3);
        map.put("Oli", 4);

        return map;
    }

    public static void removeFromMap(HashMap<String, Integer> map) {
        for(Iterator<Map.Entry<String, Integer>> it = map.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Integer> entry = it.next();
            if(entry.getValue() >= 2 && entry.getValue() <= 3) {
                it.remove();
            }
        }
    }
    // same as above
    public static void removeFromMapIf(HashMap<String, Integer> map) {
        map.entrySet().removeIf(entry -> entry.getValue() >= 2 && entry.getValue() <= 3);
    }

    public static void main(String[] args) {
        HashMap<String, Integer> map = createMap();
        removeFromMap(map);
        for (String name : map.keySet()) {
            System.out.println(name);
        }
    }

}
