package map;

import java.text.Collator;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public class TreeMapCollatorPL {
    public static void main(String[] args) {
        // standard String Collator (implementing Comparator)
        // may be used instead of Comparator (e.g. in TreeMap)

        // system default locale
        Collator collatorDefaultLocale = Collator.getInstance(Locale.getDefault());
        collatorDefaultLocale.setStrength(Collator.PRIMARY);
        // polish locale
        Collator collatorPL = Collator.getInstance(new Locale("pl","PL"));

        // using Collator
        Map<String, Integer> namesIDs = new TreeMap<>(collatorDefaultLocale) {{
            put("Łukaszewicz", 1);
            put("Lubomił", 2);
            put("Świątek", 3);
            put("Socha", 4);
        }};

        // display results
        for (Map.Entry<String, Integer> pair : namesIDs.entrySet()) {
            System.out.println(pair.getKey() + " " + pair.getValue());
        }

    }
}
