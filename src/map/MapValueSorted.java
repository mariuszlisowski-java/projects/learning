package map;

import java.util.*;
import java.util.stream.Collectors;

public class MapValueSorted {
    static Map<Integer, Integer> counts = new HashMap<>();
    static int[] array = {1, 1, 5, 6, 3, 5, 9, 0, 8, 8, 9, 8, 9, 8};

    public static void main(String[] args) {
        // count occurrences of numbers in an array
        for (int value : array) {
            counts.merge(value, 1, Integer::sum); // if key does not exist, put 1 as value
                                                        // otherwise sum 1 to the value linked to key
        }

        Map<Integer, Integer> sorted = sortByValueDecendingRef(counts);

        for (Map.Entry<Integer, Integer> pair : sorted.entrySet()) {
            System.out.println("Number " + pair.getKey() + " occurs " + pair.getValue() + " time(s)");
        }

    }

    // ascending (must be LinkedHashMap)
    private static LinkedHashMap<Integer, Integer> sortByValueAscending(Map<Integer, Integer> map) {
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(e -> e.getKey(),          // lambda
                                          e -> e.getValue(),        // lambda
                                          (e1, e2) -> e2,
                                          LinkedHashMap::new));
    }
    private static LinkedHashMap<Integer, Integer> sortByValueAscendingRef(Map<Integer, Integer> map) {
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey,        // method reference
                                          Map.Entry::getValue,      // method reference
                                          (e1, e2) -> e2,
                                          LinkedHashMap::new));
    }

    // descending (must be LinkedHashMap)
    private static LinkedHashMap<Integer, Integer> sortByValueDecendingRef(Map<Integer, Integer> map) {
        return map.entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))     // reversed here
                .collect(Collectors.toMap(Map.Entry::getKey,
                                          Map.Entry::getValue,
                                          (e1, e2) -> e2,
                                          LinkedHashMap::new));
    }

}
