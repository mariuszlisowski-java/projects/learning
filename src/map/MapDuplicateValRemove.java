package map;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class MapDuplicateValRemove {
    public static HashMap<String, String> createMap() {
        HashMap<String, String> names = new HashMap<>();

        names.put("A", "John");     // will leave
        names.put("B", "Tom");      // will leave
        names.put("C", "John");
        names.put("D", "John");
        names.put("E", "John");
        names.put("F", "Tom");
        names.put("G", "John");
        names.put("H", "John");
        names.put("I", "Alice");    // will leave
        names.put("J", "Alice");

        return names;
    }

    // removes duplicated values leaving one
    public static void removeDuplicatedValues(Map<String, String> map) {
        final Iterator<Map.Entry<String, String>> iter = map.entrySet().iterator();
        final HashSet<String> valueSet = new HashSet<String>();
        while (iter.hasNext()) {
            final Map.Entry<String, String> next = iter.next();
            if (!valueSet.add(next.getValue())) {
                iter.remove();
            }
        }
    }

    public static void main(String[] args) {
        HashMap<String, String> map = createMap();
        removeDuplicatedValues(map);
        for (String surname : map.keySet()) {
            System.out.println(surname);
        }
    }

}
