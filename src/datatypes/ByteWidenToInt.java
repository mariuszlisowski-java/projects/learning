package datatypes;

public class ByteWidenToInt {
    static byte[] buffer = new byte[] {(byte) 0xff, (byte) 0xff};

    public static void main(String[] args) {
        printArrayAsInt(buffer);    // widen
        printArrayAsByte(buffer);   // regular byte
    }

    private static void printArrayAsInt(byte[] array) {
        for (byte b : array) {
            int decimal = b & 0xff;       // bytes widen to int, need mask, prevent sign extension
            System.out.print(decimal + " ");
        }
        System.out.println();
    }

    private static void printArrayAsByte(byte[] array) {
        for (byte b : array) {
            System.out.print(b + " ");
        }
        System.out.println();
    }

}
