package datatypes;

public class ByteDefaultRange {
    // range [-128, 127] while initialized
    static byte[] buffer = new byte[] {65, 66, 67, 68, 69};

    public static void main(String[] args) {

        printArrayByte(buffer);
        printArrayHex(buffer);

        // ascii conversion
        String string = new String(buffer);
        System.out.println(string);

    }

    private static void printArrayByte(byte[] array) {
        for (byte b : array) {
            System.out.print(b + " ");
        }
        System.out.println();
    }

    private static void printArrayHex(byte[] array) {
        for (byte b : array) {
            String character = String.format("%02x", b);
            System.out.print(character + " ");
        }
        System.out.println();
    }



}
