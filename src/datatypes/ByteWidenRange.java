package datatypes;

import java.io.*;

public class ByteWidenRange {

    public static void main(String[] args) {
        // range [0 - 255] while read
        String filename = "test.hex"; // any binary file (eg. hex ffacff)

        byte[] buffer; // stores bytes [0x00, 0xff]
        try (InputStream inputStream = new FileInputStream(filename)) {
            buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
        } catch (IOException e) {
            return;
        }

        printArrayAsByte(buffer);       // signed bytes (default)
        printArrayAsInt(buffer);        // raw bytes in dec
        printArrayAsHex(buffer);        // raw bytex in hex
        printArrayAsHexString(buffer);  // another method
    }

    private static void printArrayAsByte(byte[] array) {
        for (byte b : array) {
            System.out.print(b + " ");
        }
        System.out.println();
    }

    private static void printArrayAsInt(byte[] array) {
        for (byte b : array) {
            int decimal = b & 0xff;       // bytes widen to int, need mask, prevent sign extension
            System.out.print(decimal + " ");
        }
        System.out.println();
    }

    private static void printArrayAsHex(byte[] array) {
        StringBuilder result = new StringBuilder();
        for (byte b : array) {
            int decimal = b & 0xff;       // bytes widen to int, need mask, prevent sign extension
            String hex = Integer.toHexString(decimal);
            result.append(hex).append(" ");
        }
        System.out.println(result);
    }

    private static void printArrayAsHexString(byte[] array) {
        StringBuilder result = new StringBuilder();
        for (byte b : array) {
            result.append(String.format("%02x", b)).append(" ");
        }
        System.out.println(result);
    }

}
