package znaczki;

import java.io.Serializable;

public enum Country implements Serializable {
    POLAND,
    USA,
    RUSSIA
}
