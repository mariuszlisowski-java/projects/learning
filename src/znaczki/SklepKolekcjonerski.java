package znaczki;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SklepKolekcjonerski implements Serializable {
    private String name;
    private List<Znaczek> znaczki = new ArrayList<>();

    public SklepKolekcjonerski(String name) {
        this.name = name;
    }

    public List<Znaczek> getZnaczki() {
        return znaczki;
    }

    public void dodajZnaczek(Znaczek znaczek) {
        znaczki.add(znaczek);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SklepKolekcjonerski that = (SklepKolekcjonerski) o;
        return Objects.equals(name, that.name) && Objects.equals(znaczki, that.znaczki);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, znaczki);
    }

    @Override
    public String toString() {
        return "Skep Kolekcjonerski " + name;
    }
}
