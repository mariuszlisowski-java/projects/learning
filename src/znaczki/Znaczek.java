package znaczki;

import java.io.Serializable;
import java.math.BigDecimal;

public class Znaczek implements Serializable {
    private BigDecimal price;
    private Country country;

    public Znaczek(BigDecimal price, Country country) {
        this.price = price;
        this.country = country;
    }

    @Override
    public String toString() {
        return "Znaczek{" +
                "price=" + price +
                ", country=" + country +
                '}';
    }
}
