package znaczki;

import java.io.*;
import java.math.BigDecimal;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        SklepKolekcjonerski sklep = new SklepKolekcjonerski("Mój znaczek");
        sklep.dodajZnaczek(new Znaczek(BigDecimal.valueOf(200), Country.POLAND));
        sklep.dodajZnaczek(new Znaczek(BigDecimal.valueOf(999), Country.RUSSIA));
        sklep.dodajZnaczek(new Znaczek(BigDecimal.valueOf(500), Country.USA));
        System.out.println(sklep);

        String filename = "sklep.ser";

        // write object
        OutputStream outputStream = new FileOutputStream(filename);
        ObjectOutputStream objectOutStream = new ObjectOutputStream(outputStream);
        objectOutStream.writeObject(sklep);
        objectOutStream.close();
        outputStream.close();

        // read object
        InputStream inputStream = new FileInputStream(filename);
        ObjectInputStream objectInStream = new ObjectInputStream(inputStream);
        SklepKolekcjonerski nowySklep = (SklepKolekcjonerski) objectInStream.readObject();
        inputStream.close();
        System.out.println(nowySklep);

        // output
        List<Znaczek> znaczki = nowySklep.getZnaczki();
        znaczki.stream()
                .forEach(System.out::println);

        // ???
        System.out.println(sklep.equals(nowySklep));
    }
}
