package auxilary;

public class PassByValueMutable {
    public static void main(String... mutableReference) {
        // object on heap
        StringBuilder name = new StringBuilder("Homer ");
        addSurname(name);

        // surname added
        System.out.println(name);
    }

    // name passed BY VALUE but references to an object
    static void addSurname(StringBuilder name) {
        name.append("Smith");
    }

}
