package auxilary;

public class PassByValueObject {
    public static void main(String... objectReference) {
        // object on heap
        Person person = new Person();
        changeName(person);

        // name changed
        System.out.println(person.name);
    }

    // person passed BY VALUE but as object reference
    static void changeName(Person person) {
        // copy of reference points to the same object
        person.name = "Homer";
        // no need to return (name changed)
    }

    static class Person {
        String name;
    }

}

