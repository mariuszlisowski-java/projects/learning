package auxilary;

/*
 * xor is symmetrical
 * input  XOR var -> output
 * output XOR var -> input
 */

public class EncryptDecryptXOR {
    private static byte[] buffer = new byte[]{65, 66, 67, 68, 69, 70};

    public static void main(String[] args) {

        System.out.println(new String(buffer));
        encrypt(buffer);
        decrypt(buffer);
        System.out.println(new String(buffer));
    }

    // same
    private static void decrypt(byte[] buffer) {
        for (int i = 0; i < buffer.length; i++) {
            buffer[i] ^= 1;
        }
    }
    // same
    private static void encrypt(byte[] buffer) {
        for (int i = 0; i < buffer.length; i++) {
            buffer[i] ^= 1;
        }
    }

}
