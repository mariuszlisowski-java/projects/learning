package auxilary;

public class PassByValueImmutable {
    public static void main(String... immutableCopy) {
        // string pool (immutable)
        String name = "Old";
        changeName(name);

        // no change to name
        System.out.println("In main: " + name);
    }

    // string passed BY VALUE but is immutable
    static void changeName(String name) {
        // name is a copy
        System.out.println("In method: " + name);
        // assigned here again
        name = "Homer";
        // now for garbage collecting or returning
    }
}
