package auxilary;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class MinimumExceptions {
    public static void main(String[] args) {
        List<Integer> array = new ArrayList<>() {{
            add(11);
            add(22);
            add(33);
        }};
        System.out.println("Min: " + getMinimumCollection(array));

        List<Integer> nullArray = new ArrayList<>();
        nullArray = null;
        try {
            System.out.println("Min: " + getMinimum(nullArray));
        } catch (NullPointerException exception) {
            System.out.println(exception.getMessage());
        }

        List<Integer> notInitializedArray = new ArrayList<>();
        try {
            System.out.println("Min: " + getMinimum(notInitializedArray));
        } catch (IndexOutOfBoundsException exception) {
            System.out.println(exception.getMessage());
        }
    }

    public static int getMinimumCollection(List<Integer> array) {
        if (array != null) {
            return Collections.min(array);
        } else {
            throw new NullPointerException("List is empty!");
        }
    }

    public static int getMinimum(List<Integer> array) throws IndexOutOfBoundsException {
        if (array != null) {
            int min = array.get(0);
            for (int el : array) {
                min = min < el ? min : el;
            }
            return min;
        } else {
            throw new NullPointerException("List is empty!");
        }
    }

}
