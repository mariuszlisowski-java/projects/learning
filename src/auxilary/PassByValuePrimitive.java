package auxilary;

public class PassByValuePrimitive {
    public static void main(String... primitiveCopy) {
        int age = 30;
        changeAge(age);

        // no change to age
        System.out.println("In main: " + age);
    }

    // age passed BY VALUE but primitive
    static void changeAge(int age) {
        // age is a copy
        System.out.println("In method: " + age);
        // assigned here again
        age = 55;
        // now for garbage collecting or returning
    }
}
