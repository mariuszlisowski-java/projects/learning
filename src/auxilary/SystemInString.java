package auxilary;

import java.io.*;

public class SystemInString {

    public static void main(String[] args) throws IOException {
        String greetings = "Hi! My name is Amigo!\n" +
                           "I'm learning Java on the CodeGym website.\n" +
                           "One day I will become a cool programmer!\n";

        // convert string to bytes
        byte[] bytes = greetings.getBytes();
        InputStream inputStream = new ByteArrayInputStream(bytes);

        // set 'in' variable for 'inputStream'
        System.setIn(inputStream);

        // read using modified variable
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String str;
        while ((str = reader.readLine())!= null) {
            System.out.println(str);
        }

    }

}
