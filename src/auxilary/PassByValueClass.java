package auxilary;

public class PassByValueClass {
    public static class Point {
        int x, y;
        Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    public static void main(String[] args) {
        Point point = new Point(1, 1);

        doNotChangePoint(point);
        System.out.println(point.x + ", " + point.y);

        changePoint(point);
        System.out.println(point.x + ", " + point.y);

        Point newPoint = returnNewPoint(point);
        System.out.println(newPoint.x + ", " + newPoint.y);


    }

    private static void changePoint(Point p) {
        p.x = 9;
        p.y = 9;
    }

    private static void doNotChangePoint(Point p) {
        p = new Point(9, 9);
        p.x = 9;
        p.y = 9;
    }

    private static Point returnNewPoint(Point p) {
        p = new Point(9, 9);
        p.x = 5;
        p.y = 5;

        return p;
    }

}
