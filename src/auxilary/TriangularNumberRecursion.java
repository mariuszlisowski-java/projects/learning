package auxilary;

public class TriangularNumberRecursion {
    public static void main(String[] args) {
        int number = triangularNumber(6);
        System.out.println(number);
    }

    public static int triangularNumber(int n) {
        return n == 1 ? 1 : n + triangularNumber(n - 1);
    }

}

//  n = 6 -> 5 -> 4 -> 3 -> 2 -> 1 // start at n = 1
//
//  1 triangularNumber(n = 1) { return 1;      }
//  3 triangularNumber(n = 2) { return n + 1;  } // 2 + 1
//  6 triangularNumber(n = 3) { return n + 3;  } // 3 + 3
// 10 triangularNumber(n = 4) { return n + 6;  } // 4 + 6
// 15 triangularNumber(n = 5) { return n + 10; } // 5 + 10
// 21 triangularNumber(n = 6) { return n + 15; } // 6 + 15

