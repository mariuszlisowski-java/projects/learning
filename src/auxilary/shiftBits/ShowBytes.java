package auxilary.shiftBits;

class ShowBytes {
    // attribute
    int numbits;
    // constructor
    ShowBytes(int numbits) {
        this.numbits = numbits;
    }
    // method
    void showBytes(long value) {
        long mask = 1;
        mask <<= numbits - 1;
        int spacer = 0;

        System.out.print("Decimal: " + value + "\t| Binary: ");
        for (; mask != 0; mask >>>= 1) {
            if ((value & mask) != 0) {
                System.out.print("1");
            } else {
                System.out.print("0");
            }
            ++spacer;
            if (spacer % 8 == 0) {
                System.out.print(" ");
                spacer = 0;
            }
        }
        System.out.println();
    }
} // ShowBytes

class ShowBytesDemo {
    public static void main(String[] args) {
        ShowBytes byte_size = new ShowBytes(8);
        ShowBytes short_size = new ShowBytes(16);
        ShowBytes int_size = new ShowBytes(32);
        ShowBytes long_size = new ShowBytes(64);

        byte_size.showBytes(Byte.MAX_VALUE);
        short_size.showBytes(Short.MAX_VALUE);
        int_size.showBytes(Integer.MAX_VALUE);
        long_size.showBytes(Long.MAX_VALUE);
    }
} // ShowBytesDemo
