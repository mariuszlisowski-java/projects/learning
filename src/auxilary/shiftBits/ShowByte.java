package auxilary.shiftBits;

public class ShowByte {

    public static void main(String[] args) {
        printByte(2);
        printByte(128);
        printByte(64);
        printByte(255);
        printByte(256);
    }

    public static void printByte(int decimal) {
        if (decimal <= 255) {
            System.out.print("Decimal: " + decimal + "\t| Binary: ");
            for (int j = 128; j > 0; j /= 2) {
                if ((decimal & j) != 0) {
                    System.out.print("1");
                } else {
                    System.out.print("0");
                }
            }
            System.out.println();
        } else {
            System.out.println("Max byte value is decimal 255!");
        }

    }
}
