package auxilary;

import java.util.stream.IntStream;

public class FizzBuzz {
    public static void main(String[] args) {
        fizzBuzzB(20);
    }

    public static void fizzBuzzA(int maxRange) {
        for (int i = 1; i <= maxRange; i++) {
            String result = "";
            if (i % 3 == 0) { result = "Fizz"; }
            if (i % 5 == 0) { result += "Buzz"; }
            if (result.isEmpty()) { result += i; }
            System.out.println(result);
        }
    }

    public static void fizzBuzzB(int maxRange) {
        for (int i = 1; i <= maxRange; i++) {
            System.out.println((i % 15 != 0 ?
                                    (i % 5 != 0 ?
                                       (i % 3 != 0 ? String.valueOf(i) : "Fizz")
                                       : "Buzz"
                                    )
                               : "FizzBuzz")
            );
        }
    }

    public static void fizzBuzzC(int maxRange) {
        IntStream.rangeClosed(1, maxRange)
                 .mapToObj(i -> {
                     if (i % (3 * 5) == 0) {
                         return "FizzBuzz";
                     } else if (i % 3 == 0) {
                         return "Fizz";
                     } else if (i % 5 == 0) {
                         return "Buzz";
                     } else {
                         return Integer.toString(i);
                     }
                 })
                 .forEach(System.out::println);
    }

}
