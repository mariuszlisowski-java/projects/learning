package auxilary;

public class BubbleSortFromRight {
    public static void main(String[] args) {
//        String[] array = { "abcdef", "ab", "abcdefg", "abcde", "abcd", "abc"};

        String[] array = { "f", "d", "a", "c", "e", "b"}; // 5 iterations !!!

        printArray(array);
        System.out.println();
        sort(array);
        printArray(array);
    }

    public static void sort(String[] array) {
        for (int i = 1; i < array.length; ++i) {
            System.out.println("Outer iteration: " + i);
            for (int j = array.length - 1; j >= i; --j) {
                System.out.println(array[j] + " : " + array[j-1]);
                if (array[j-1].compareTo(array[j]) > 0) {
                    System.out.println("swapped!");
                    String temp = array[j-1];
                    array[j-1] = array[j];
                    array[j] = temp;
                    printArray(array);
                }
            }
            System.out.println();
        }
    }

    public static void printArray(String[] array) {
        for (String x : array) {
            System.out.print(x + "|");
        }
        System.out.println();
    }

}
