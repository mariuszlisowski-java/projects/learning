package auxilary;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MaximumOccurencesArray {
    static Map<Integer, Integer> counts = new HashMap<>();
    static int[] array = {1, 1, 1, 1,       // max 4 occurrences
                          8, 8, 8, 8,       // same here
                          2, 2, 2,
                          9, 9};


    public static void main(String[] args) {
        // count occurrences of each number
        for (int value : array) {
            counts.merge(value, 1, Integer::sum); // key (number), value (occurrences)
        }

        // find max occurrence in a map
        int maxValue = 0;
        for (Integer value : counts.values()) {
            if (value > maxValue) {
                maxValue = value;
            }
        }

        // get one or more keys with the same (max) value
        Set<Integer> maxOccurences = getKeysByValue(counts, maxValue);

        for (Integer value : maxOccurences) {
            System.out.print(value + " ");
        }
    }

    private static <T, E> Set<T> getKeysByValue(Map<T, E> map, E value) {
        Set<T> keys = new HashSet<>();

        for (Map.Entry<T, E> pair : map.entrySet()) {
            if (pair.getValue().equals(value)) {
                keys.add(pair.getKey());
            }
        }

        return keys;
    }

}
