package auxilary;

public class BubbleSortFromLeftOpt {
    public static void main(String[] args) {
//        String[] array = { "abcdef", "ab", "abcdefg", "abcde", "abcd", "abc"};

        String[] array = { "f", "d", "a", "c", "e", "b"}; // 4 iterations !!!

        printArray(array);
        System.out.println();
        sort(array);
        printArray(array);
    }

    public static void sort(String[] array) {
        for (int i = 0; i < array.length - 1; ++i) {
            System.out.println("Outer iteration: " + i);
            for (int j = 0; j < array.length - 1 - i; ++j) {
                System.out.println(array[j] + " : " + array[j+1]);
                if (array[j].compareTo(array[j+1]) > 0) {
                    System.out.println("swapped!");
                    String temp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = temp;
                    printArray(array);
                }
            }
            System.out.println();
        }
    }

    public static void printArray(String[] array) {
        for (String x : array) {
            System.out.print(x + "|");
        }
        System.out.println();
    }

    public BubbleSortFromLeftOpt() {
    }
}
