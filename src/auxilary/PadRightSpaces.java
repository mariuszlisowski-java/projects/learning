package auxilary;

public class PadRightSpaces {

    public static void main(String[] argv) {
        System.out.println("\"" + rightPadSpaces("string", 8) + "\"");
        System.out.println("\"" + rightPadSpaces("string", 9) + "\"");
        System.out.println("\"" + rightPadSpaces("string", 10) + "\"");
    }

    public static String rightPadSpaces(String str, int maxLength) {
        return String.format("%1$-" + maxLength + "s", str);
    }

}
