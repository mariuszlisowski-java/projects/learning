package auxilary;

public class PassByValueArray {
    public static void main(String[] arrayReference) {
        // arr as a reference to an ascending array
        int[] arr = {1, 2, 3};

        // no change to arr reference
        replaceArray(arr);
        printArray(arr);

        // arr reference overwritten
        arr = replaceArray(arr);
        printArray(arr);

        //
//        arraySort(arr);
//        printArray(arr);

    }

    // array passed BY VALUE but references to the original array
    public static int[] replaceArray(int[] array) {
        // reference now points to a newly created array
        array = new int[]{1, 2, 3, 4, 5, 6};
        // and its returned
        return array;
    }

    public static void arraySort(int[] array) {
//        Arrays.asList(array);
        // no need to return (array sorted)
    }

    public static void printArray(int[] array) {
        for (int el : array) {
            System.out.print(el + " ");
        }
        System.out.println();
    }

}
