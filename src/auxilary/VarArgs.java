package auxilary;

public class VarArgs {
    public static void main(String[] args) {
        System.out.println(sum(1));
        System.out.println(sum(1, 1));
        System.out.println(sum(1, 1, 1));
        System.out.println(sum(1, 1, 1, 1));
    }

    private static int sum(int... numbers) {
        int sum = 0;
        for (int num : numbers ) {
            sum += num;
        }

        return sum;
    }
}
