package auxilary;

public class PadRightZeros {
    public static void main(String[] argv) {
        System.out.println("\"" + rightPadZeroes("string", 8) + "\"");
        System.out.println("\"" + rightPadZeroes("string", 9) + "\"");
        System.out.println("\"" + rightPadZeroes("string", 10) + "\"");
    }

    public static String rightPadZeroes(String str, int num) {
        return String.format("%1$-" + num + "s", str).replace(' ', '0');
    }

}
