package auxilary;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Random;

public class MeasureTime {
    private static final int MIN_RANGE_SECS = 1;
    private static final int MAX_RANGE_SECS = 3;
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");

    public static void main(String[] args) throws InterruptedException {
        // start measurement
        Date startTime = new Date();

        // operation to measure
        Thread.sleep(new Random().nextInt((MAX_RANGE_SECS - MIN_RANGE_SECS) * 1000) +
                     MIN_RANGE_SECS * 1000); // [1, 2) secs

        // stop measurement
        Date endTime = new Date();

        // compute difference
        double difference = (double) (endTime.getTime() - startTime.getTime()) / 1000 * 1.0;

        // display results
        System.out.println("Operation took: " +
                           DECIMAL_FORMAT.format(difference) +
                           " second(s)");
    }


}
