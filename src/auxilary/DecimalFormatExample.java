package auxilary;

import java.text.DecimalFormat;

public class DecimalFormatExample {
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");

    public static void main(String[] args) {
        double pi = Math.PI;

        // no formatting
        System.out.println(pi);

        // formatting
        System.out.println(DECIMAL_FORMAT.format(pi));
    }
}
