package classes.staticnested;

public class OuterClass {
    public static void main(String[] args) {
        // here: no need to assess via OuterClass
        StaticNestedClass staticNestedClassA = new StaticNestedClass();
    }

    // must be static to create an object without OuterClass object
    static class StaticNestedClass {}

}
