package classes.staticnested;

public class Main {
    public static void main(String[] args) {
        // accessing the OuterClass while creating an object
        OuterClass.StaticNestedClass staticNestedClassB =
                new OuterClass.StaticNestedClass();
    }
}
