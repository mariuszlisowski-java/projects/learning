package classes.inner;

public class OuterClass {
    public static void main(String[] args) {
        // creating within OuterClass
        OuterClass outerClassObject = new OuterClass();
        InnerClass innerClass = outerClassObject.new InnerClass();
    }

    // no need to be static as Inner is created out of Outer
    class InnerClass {}
}
