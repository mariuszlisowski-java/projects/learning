package classes.inner;

public class Main {
    public static void main(String[] args) {
        // creating outside OuterClass
        OuterClass outerClassObject = new OuterClass();
        OuterClass.InnerClass innerClassObject = outerClassObject.new InnerClass();
    }
}
