package calendar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class CalendarBC {
    public static void main(String[] args) {
        Calendar cannae = new GregorianCalendar(216, Calendar.AUGUST, 2);
        cannae.set(Calendar.ERA, GregorianCalendar.BC);

        DateFormat df = new SimpleDateFormat("MMM dd, yyy GG");
        System.out.println(df.format(cannae.getTime()));
    }
}
