package calendar;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class CalendarGet {
    public static void main(String[] args) {
        Calendar calendar = new GregorianCalendar(2020, Calendar.JANUARY , 1);
        calendar.set(Calendar.HOUR, 20);
        calendar.set(Calendar.MINUTE, 1);
        calendar.set(Calendar.SECOND, 59);

        System.out.println("Year: " + calendar.get(Calendar.YEAR));
        System.out.println("Month: " + calendar.get(Calendar.MONTH));
        System.out.println("Day: " + calendar.get(Calendar.DAY_OF_MONTH));

        System.out.println("Week in the month: " + calendar.get(Calendar.WEEK_OF_MONTH));

        System.out.println("Hours: " + calendar.get(Calendar.HOUR));
        System.out.println("Minutes: " + calendar.get(Calendar.MINUTE));
        System.out.println("Seconds: " + calendar.get(Calendar.SECOND));
        System.out.println("Milliseconds: " + calendar.get(Calendar.MILLISECOND));

    }

}
