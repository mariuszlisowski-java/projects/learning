package calendar;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class CalendarAdd {
    public static void main(String[] args) {
        Calendar calendar = new GregorianCalendar(2020, Calendar.JANUARY , 1);
        calendar.set(Calendar.HOUR, 19);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 59);

        calendar.add(Calendar.HOUR, 2); // 19 + 2
        calendar.add(Calendar.MONTH, -2); // subtract: pass a negative number

        // the year changed to the previous
        System.out.println(calendar.getTime());
    }

}
