package time;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ExecutionTime {
    final static int REPETITIONS = 100_000;

    public static void main(String[] args) {
        // slow add(index), remove
        // fast get, set, add(end)
        ArrayList arrayList = new ArrayList();

        // fast add, add(index), remove
        // slow get, set
        LinkedList linkedList = new LinkedList();

        /* add(index) method */
        // ArrayList (slow)
        System.out.println("Adding " + REPETITIONS + " items to ArrayList: " +
                           calculateAddingTimeInMilisecs(arrayList) + " miliseconds");
        // LinkedList (fast)
        System.out.println("Adding " + REPETITIONS + " items to LinkedList: " +
                           calculateAddingTimeInMilisecs(linkedList) + " miliseconds");

        /* get method */
        // ArrayList (fast)
        System.out.println("Getting " + REPETITIONS + "th object from ArrayList: " +
                           calculateGettinTimeInNanosecs(arrayList) + " nanoseconds");
        // LinkedList (slow)
        System.out.println("Getting " + REPETITIONS + "th object from LinkedList: " +
                           calculateGettinTimeInNanosecs(linkedList) + " nanoseconds");


    }

    public static long calculateAddingTimeInMilisecs(List list) {
        long startTime = System.currentTimeMillis();
        // method being tested
        addAtIndexToList(list);
        long stopTime = System.currentTimeMillis();

        return stopTime - startTime;
    }
    public static long calculateGettinTimeInNanosecs(List list) {
        long startTime = System.nanoTime();
        // method being tested
        getFromList(list);
        long stopTime = System.nanoTime();

        return stopTime - startTime;
    }

    public static void addAtIndexToList(List list) {
        for (int i = 0; i < REPETITIONS; i++) {
            list.add(0, new Object());
        }
    }

    public static Object getFromList(List list) {
        return list.get(REPETITIONS - 1);
    }
}
